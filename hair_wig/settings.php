<?php

/*
 Plugin Name: Keelahair Custom Hair Wig 
 Description: Hair Wig Buying System
 Version: 1.0.0
 Plugin URI: http://logicsbuffer.com/
 Author: https://www.fiverr.com/mustafajamalatt
*/


if ( ! defined( 'ABSPATH' ) )
	exit;

if ( isWooCommerceActive()) {

	register_activation_hook( __FILE__,  'install' );
	add_action('wp_enqueue_scripts', 'stern_taxi_fares_script_front_css');
	//add_action('wp_enqueue_scripts', 'stern_taxi_fares_script_front_js');
	add_action('wp_enqueue_scripts', 'stern_taxi_fares_script_front_js1');

	//add_action('admin_enqueue_scripts', 'stern_taxi_fares_script_back_js');

	add_action('admin_enqueue_scripts', 'stern_taxi_fares_script_back_css');
	//add_action( 'plugins_loaded', 'myplugin_load_textdomain' );

	add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'ts_add_plugin_action_links' );
	add_action('init', 'stern_taxi_fare_register_shortcodes1');
	add_action('woocommerce_checkout_update_order_meta',function( $order_id, $posted ) {
	$ins_pdf_imageset = get_option( 'ins_pdf_image');

    $order = wc_get_order( $order_id );
    $order->update_meta_data( 'policy_data', $ins_pdf_imageset);
    $order->save();
	} , 10, 2);   

	include 'controller/stern_taxi_fare_admin.php';
	//include 'templates/form/showForm1.php';
	include 'templates/form/showForm2.php';
	//include 'templates/checkout/checkout.php';
	include 'controller/functions.php';
	//include 'templates/form/showResults.php';
	include 'controller/stern_taxi_fare_ajax.php';
	include 'controller/stern_taxi_fare_ajax_admin.php';
	//include 'templates/form/showButtonCheckOut.php';
	include 'templates/admin/settings.php';
	//include 'templates/admin/woocommerceStatus.php';
	
/* 	if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/ReduxFramework/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/ReduxFramework/ReduxCore/framework.php' );
	}
	if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . 'ReduxFramework/ReduxCore/sample-config.php' ) ) {
		require_once( dirname( __FILE__ ) . '/ReduxFramework/ReduxCore/sample-config.php' );
	} */
	require_once( dirname( __FILE__ ) . '/ReduxFramework/sample/wig-config.php' );

} else {

    function stern_ticketing_events_admin_notice() { ?>
        <div class="error">
            <p><?php _e( 'Insurance Qoute: Please Install WooCommerce First before activating this Plugin. You can download WooCommerce from <a href="http://wordpress.org/plugins/woocommerce/">here</a>.', 'stern-ticketing-events' ); ?></p>
        </div>
    <?php }
    add_action( 'admin_notices', 'stern_ticketing_events_admin_notice' );
	//deactivate_plugins( plugin_basename( __FILE__ ) );

	//wp_die( _e( 'Please Install WooCommerce First before activating this Plugin. You can download WooCommerce from <a href="http://wordpress.org/plugins/woocommerce/">here</a>.', 'stern-ticketing-events' ) );

}
add_action( 'wp_ajax_nopriv_post_love_add_love', 'post_love_add_love' );
add_action( 'wp_ajax_post_love_add_love', 'post_love_add_love' );

function post_love_add_love() {
	$love = get_post_meta( $_REQUEST['post_id'], 'post_love', true );
	$love++;
	update_post_meta( $_REQUEST['post_id'], 'post_love', $love );
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { 
		echo $love;
		die();
	}
	else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}

add_action( 'wp_ajax_nopriv_ajax_product', 'ajax_product' );
add_action( 'wp_ajax_ajax_product', 'ajax_product' );


function stern_taxi_fare_register_shortcodes1() {
   add_shortcode('show_hair_wig', 'stern_taxi_fare2');
   add_shortcode('insurance-qoute-system', 'stern_taxi_fare1');
   add_action( 'user_register', 'myplugin_registration_save', 10, 1 );
}

function myplugin_registration_save( $user_id ) {
    $full_name_key = "full_name";
	$current_order_id = get_option("c_id");	
	update_field($full_name_key, $full_name_key, [$current_order_id]); 	
}


function stern_taxi_fare1($atts) {
	ob_start();
	launchForm($atts);
	return ob_get_clean();
}

function stern_taxi_fare2($atts) {
	ob_start();
	//launchFormTpl($atts);
	showForm2($atts);
	return ob_get_clean();
}


function isWooCommerceActive() {
	if(is_multisite() ) {
		return true;
	} else {
		if(
			//is_plugin_active( 'woocommerce/woocommerce.php') or
			in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) )
		//	or is_plugin_active_for_network( 'woocommerce/woocommerce.php')
		//	or is_network_only_plugin('woocommerce/woocommerce.php')
		) {
			return true;
		} else {
			return false;
		}
	}
}

 function install() {
	$exist = get_option('stern_taxi_fare_product_id_wc');
	//saveVersion();
	//sendInfosDebug();
	if($exist){
		if ( get_post_status ( $exist ) ) {
			return true;
		} else {
			createProductAndSaveId();
		}
	} else {
		createProductAndSaveId();
	}
}

//Open Checkout in new tab

//Add back button in Checkout
/* add_action( 'woocommerce_after_checkout_form', 'my_second_function_sample', 10 );
function my_second_function_sample() {
  global $product;
  echo ' <button type="button" id="checkout_backbtn" onclick="history.back();"> Go back </button> '; 
} */



function stern_taxi_fares_script_back_css() {

		//wp_register_style( 'stern_jquery_ui_css', plugins_url('css/jquery-ui.css', __FILE__ ));
		//wp_enqueue_style( 'stern_jquery_ui_css');

		//wp_register_style( 'stern_fullCalendar_MIN_css', plugins_url('css/fullcalendar.min.css', __FILE__ ));
		//wp_enqueue_style( 'stern_fullCalendar_MIN_css');
}

function stern_taxi_fares_script_front_css() {
		/* CSS */

		wp_register_style('stern_bootstrapValidatorMinCSS', plugins_url('css/bootstrapValidator.min.css',__FILE__));
		wp_register_style('smartForms', plugins_url('css/smart-forms.css',__FILE__));

        wp_enqueue_style('smartForms');
        wp_enqueue_style('stern_bootstrapValidatorMinCSS');

		wp_register_style( 'ins_steps_css', plugins_url('css/jquery.steps.css', __FILE__ ));
		wp_enqueue_style( 'ins_steps_css');

		wp_register_style( 'ins_stepper', plugins_url('css/jquery.stepper.css', __FILE__ ));
		wp_enqueue_style( 'ins_stepper');

		wp_register_style( 'ins_sfcustom', plugins_url('css/smartforms-custom.css', __FILE__ ));
		wp_enqueue_style( 'ins_sfcustom');

		wp_register_style( 'ins_cssptable1', plugins_url('cssptable/pricing-widget-structure.css', __FILE__ ));
		wp_enqueue_style( 'ins_cssptable1');

		wp_register_style( 'ins_cssptable2', plugins_url('cssptable/pricing-widget-settings.css', __FILE__ ));
		wp_enqueue_style( 'ins_cssptable2');

		wp_register_style( 'ins_cssptable3', plugins_url('cssptable/pricing-widget-theme.css', __FILE__ ));
		wp_enqueue_style( 'ins_cssptable3');

		wp_register_style( 'ins_cssptable4', plugins_url('cssptable/pricing-widget-responsive.css', __FILE__ ));
		wp_enqueue_style( 'ins_cssptable4');
		
		wp_register_style( 'ins_smartaddon', plugins_url('css/smart-addons.css', __FILE__ ));
		wp_enqueue_style( 'ins_smartaddon');

		wp_register_style( 'ins_smartforms_modal', plugins_url('css/smartforms-modal.css', __FILE__ ));
		wp_enqueue_style( 'ins_smartforms_modal');
			
		wp_register_style( 'ins_popupn', plugins_url('css/popup.min.css', __FILE__ ));
		wp_enqueue_style( 'ins_popupn');

        wp_register_style('stern_taxi_fare_datetimepicker', plugins_url('css/bootstrap-datetimepicker.css',__FILE__));
        wp_enqueue_style('stern_taxi_fare_datetimepicker');

        wp_register_style('hair_wig_circle', plugins_url('css/cs-skin-circular.css',__FILE__));
        wp_enqueue_style('hair_wig_circle');

        wp_register_style('hair_wig_skin_boxes', plugins_url('css/cs-skin-boxes.css',__FILE__));
        wp_enqueue_style('hair_wig_skin_boxes');
       
	    wp_register_style('hair_wig_cs_select', plugins_url('css/cs-select.css',__FILE__));
        wp_enqueue_style('hair_wig_cs_select');

        wp_register_style('ins_style', plugins_url('css/ins_style.css',__FILE__));
        wp_enqueue_style('ins_style');
		wp_register_style('tabel_style', plugins_url('css/tabel_style.css',__FILE__));
        wp_enqueue_style('tabel_style');
}



/**
 * Add the field to the checkout page
 */


add_action('woocommerce_checkout_before_order_review', 'customise_checkout_field');

function customise_checkout_field($checkout){
	
	foreach( WC()->cart->get_cart() as $cart_item ){
		$product_id = $cart_item['product_id'];
	}
	
	//Get data from PHP Sessions
	$price_image =  get_option('price_image');
	$price_length = get_option('price_length');
	$price_texture = get_option('price_texture');
	$price_color = get_option('price_color');
	$custom_wig = get_option('custom_wig');
	if($product_id == 2781){
	?>		
	<!-- Sidebar right -->
	
	<div id="bought_things" class="rightsidebar">
		<h3 style="text-align:center;">Selected Items</h3>			
		<div class="base_price_block ">								
		
		<p>Custom Wig:</p>
		<p>Length:</p>
		<p>Texture:</p>				
		<p>Color:</p>				
		</div>
		<div class="base_price_block">								
		<p><?php echo "$".$price_image;?></p>
		<p><?php echo "$".$price_length;?></p>
		<p><?php echo "$".$price_texture;?></p>						
		<p><?php echo "$".$price_color;?></p>						
		</div>
			
	</div>
	
	<!-- end Sidebar right -->	
	<?php
	}
} 

//add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
//add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );

//add_filter('woocommerce_thankyou_order_received_text', 'woo_change_order_received_text', 10, 2 );

//Plugin Woocommerce Support

function myplugin_plugin_path() {

  // gets the absolute path to this plugin directory

  return untrailingslashit( plugin_dir_path( __FILE__ ) );
}
add_filter( 'woocommerce_locate_template', 'myplugin_woocommerce_locate_template', 10, 3 );



function myplugin_woocommerce_locate_template( $template, $template_name, $template_path ) {
  global $woocommerce;

  $_template = $template;

  if ( ! $template_path ) $template_path = $woocommerce->template_url;

  $plugin_path  = myplugin_plugin_path() . '/woocommerce/';

  // Look within passed path within the theme - this is priority
  $template = locate_template(

    array(
      $template_path . $template_name,
      $template_name
    )
  );

  // Modification: Get the template from this plugin, if it exists
  if ( ! $template && file_exists( $plugin_path . $template_name ) )
    $template = $plugin_path . $template_name;

  // Use default template
  if ( ! $template )
    $template = $_template;

  // Return what we found
  return $template;
}


//Plugin Woocommerce Support





function my_ajax_rt() {
}


	wp_enqueue_script( 'love', plugins_url( '/love.js', __FILE__ ), array('jquery'), '1.0', true );

	wp_localize_script( 'love', 'postlove', array('ajax_url' => admin_url( 'admin-ajax.php' )));

function stern_taxi_fares_script_front_js1() {
	
       	wp_register_script('stern_taxi_fare', plugins_url('js/stern_taxi_fare.js', __FILE__ ),array('jquery'));
		wp_localize_script('stern_taxi_fare', 'my_ajax_object',			array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_toll',		array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_picker',		array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_suitcases',	array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_carFare',	array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'ajax_obj_calendar',			array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_enqueue_script('stern_taxi_fare', plugins_url('js/stern_taxi_fare.js', __FILE__ ) );

	
}
function stern_taxi_fares_script_front_js() {

		//wp_register_script('stern_taxi_fare_fullcalendar_front_js', plugins_url('js/stern_taxi_fare_fullcalendar_front.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_taxi_fare_fullcalendar_front_js');
        
		wp_register_script('stern_bootstrapValidatorMin', plugins_url('js/bootstrapValidator.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('stern_bootstrapValidatorMin');

	  //  wp_register_script('ins_additionalmethords', plugins_url('js/additional-methods.min.js', __FILE__ ),array('jquery'));
       // wp_enqueue_script('ins_additionalmethords');


	
        wp_register_script('ins_steps', plugins_url('js/jquery.steps.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_steps');

        wp_register_script('ins_jqueryforms', plugins_url('js/jquery.form.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryforms');

        //wp_register_script('ins_jquery19', plugins_url('js/jquery-1.9.1.min.js', __FILE__ ),array('jquery'));
       // wp_enqueue_script('ins_jquery19');

        wp_register_script('ins_utils', plugins_url('js/utils.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_utils');

        //wp_register_script('ins_jqueryuicombo', plugins_url('js/jquery-ui-combo.min.js', __FILE__ ),array('jquery'));
       // wp_enqueue_script('ins_jqueryuicombo');

        wp_register_script('ins_jqueryuicostom', plugins_url('js/jquery-ui-custom.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryuicostom');
        
		wp_register_script('ins_jquerystepper', plugins_url('js/jquery.stepper.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jquerystepper');
		
		wp_register_script('ins_jqueryplaceholder', plugins_url('js/jquery.placeholder.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryplaceholder');
		
		wp_register_script('ins_jqueryvalidate', plugins_url('js/jquery.validate.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryvalidate');

		wp_register_script('ins_formsmodel', plugins_url('js/smartforms-modal.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_formsmodel');

		wp_register_script('ins_select2', plugins_url('js/select2.full.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_select2');

		wp_register_script('ins_touch', plugins_url('js/jquery-ui-touch-punch.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_touch');
		
		wp_register_script('ins_maskedinput', plugins_url('js/jquery.maskedinput.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_maskedinput');

		wp_register_script('ins_smartforms', plugins_url('js/smart-form.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_smartforms');

		wp_register_script('ins_jqueryautotab', plugins_url('js/jquery.autotab.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryautotab');

		wp_register_script('ins_tellinput', plugins_url('js/intlTelInput.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_tellinput');

 		wp_register_script('drpzone_uploader', plugins_url('js/dropzone.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('drpzone_uploader');
		
		/*
		wp_register_script('ins_ui_touch', plugins_url('js/jquery-ui-touch-punch.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_ui_touch');
		*/
		
				
		//wp_register_script('ins_jquerymin', plugins_url('js/jquery.min', __FILE__ ),array('jquery'));
       //wp_enqueue_script('ins_jquerymin');


		
        wp_register_script('stern_bootstrapValidatorJS', plugins_url('js/bootstrapValidator.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('stern_bootstrapValidatorJS');
		

        //wp_register_script('stern_bootstrapselectMin', plugins_url('js/bootstrap-select.min.js', __FILE__ ),array('jquery'));
       // wp_enqueue_script('stern_bootstrapselectMin');

/*
        wp_register_script('stern_bootstrapselect', plugins_url('js/bootstrap-select.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('stern_bootstrapselect');
*/



		//wp_register_script('stern_bootstrapMoment', plugins_url('js/moment-with-locales.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_bootstrapMoment');

		//wp_register_script('stern_jquery_ui', plugins_url('js/jquery-ui.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_jquery_ui');

		//wp_register_script('stern_bootstrapMinjs', plugins_url('js/bootstrap.min.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_bootstrapMinjs');


		/* if(get_option('stern_taxi_fare_lib_bootstrap_js')=="true"){
			wp_register_script('stern_taxi_fare_lib_bootstrap_js', plugins_url('/bootstrap/js/bootstrap.js', __FILE__ ),array('jquery'));
			wp_enqueue_script('stern_taxi_fare_lib_bootstrap_js');

		} */

		//wp_register_script('stern_bootstrapdatetimepickerMin', plugins_url('js/bootstrap-datetimepicker.min.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_bootstrapdatetimepickerMin');

		//wp_register_script('stern_bootstrapdatetimepicker', plugins_url('js/bootstrap-datetimepicker.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_bootstrapdatetimepicker');

/*

		wp_register_script('stern_bootstrap_datetimepickerFR', plugins_url('js/bootstrap-datetimepicker.fr.js', __FILE__ ),array('jquery'));
		wp_enqueue_script('stern_bootstrap_datetimepickerFR');

  */

		//wp_register_script('stern_jquery_fullCalendarJS', plugins_url('js/fullcalendar.min.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_jquery_fullCalendarJS');

	//	wp_register_script( 'stern-bootstrapwp', plugins_url('/js/bootstrap-wp.js', __FILE__ ),array('jquery'));
	//	wp_enqueue_script( 'stern-bootstrapwp');


 
       	wp_register_script('stern_taxi_fare', plugins_url('js/stern_taxi_fare.js', __FILE__ ),array('jquery'));
		wp_localize_script('stern_taxi_fare', 'my_ajax_object',			array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_toll',		array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_picker',		array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_suitcases',	array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_carFare',	array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'ajax_obj_calendar',			array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

		wp_enqueue_script('stern_taxi_fare', plugins_url('js/stern_taxi_fare.js', __FILE__ ) );
 

		//add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
		 
/* 		function custom_override_checkout_fields( $fields ) {
			//unset($fields['billing']['billing_first_name']);
			//unset($fields['billing']['billing_last_name']);
			//unset($fields['billing']['billing_company']);
			//unset($fields['billing']['billing_address_1']);
			//unset($fields['billing']['billing_address_2']);
			//unset($fields['billing']['billing_city']);
			//unset($fields['billing']['billing_postcode']);
			//unset($fields['billing']['billing_country']);
			unset($fields['billing']['billing_state']);
			//unset($fields['billing']['billing_phone']);
			//unset($fields['order']['order_comments']);
			unset($fields['billing']['billing_address_2']);
			unset($fields['billing']['billing_postcode']);
			unset($fields['billing']['billing_company']);
			//unset($fields['billing']['billing_last_name']);
			//unset($fields['billing']['billing_email']);
			//unset($fields['billing']['billing_city']);
			return $fields;
		} */
		/* add_filter( 'woocommerce_currency_symbol', 'wc_change_uae_currency_symbol', 10, 2 );

		function wc_change_uae_currency_symbol( $currency_symbol, $currency ) {
			switch ( $currency ) {
				case 'AED':
					$currency_symbol = 'AED';
				break;
			}

			return $currency_symbol;
		} */
	    // load bootstrap jquery
		//    wp_enqueue_script( 'stern-jquery', plugins_url('/js/jquery.min.js', __FILE__ ),array('jquery'));
	//}
}
/* add_filter( 'woocommerce_countries_inc_tax_or_vat', function () {
  return __( 'GST', 'woocommerce' );
}); */
/**
 * Load plugin textdomain.
 *
 * @since 1.0.0
 */
 
/* function myplugin_load_textdomain() {
  	$locale = apply_filters( 'plugin_locale', get_locale(), 'stern-taxi_fare' );
	$dir    = trailingslashit( WP_LANG_DIR );
	load_textdomain( 'stern-taxi-fare', $dir . 'stern-taxi_fares/stern_taxi_fare-' . $locale . '.mo' );
	load_plugin_textdomain( 'stern_taxi_fare', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

} */