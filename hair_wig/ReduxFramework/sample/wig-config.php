<?php

    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "redux_demo";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Hair  Options', 'redux-framework-demo' ),
        'page_title'           => __( 'Hair Options', 'redux-framework-demo' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => true,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => __( 'Documentation', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => __( 'Support', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => __( 'Extensions', 'redux-framework-demo' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://www.linkedin.com/company/redux-framework',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'redux-framework-demo' ), $v );
    } else {
        $args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'redux-framework-demo' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields

  /*   Redux::setSection( $opt_name, array(
        'title' => __( 'Pricing Options', 'redux-framework-demo' ),
        'id'    => 'basic',
        'desc'  => __( 'Basic fields as subsections.', 'redux-framework-demo' ),
        'icon'  => 'el el-home'
    ) ); */
    
	Redux::setSection( $opt_name, array(
        'title'  => __( 'Pricing Options', 'redux-framework-demo' ),
        'id'     => 'basic',
        'desc'   => __( 'Basic field with no subsections.', 'redux-framework-demo' ),
        'icon'   => 'el el-home',
       
    ) );
	

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Step 1', 'redux-framework-demo' ),
        'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="//docs.reduxframework.com/core/fields/textarea/" target="_blank">//docs.reduxframework.com/core/fields/textarea/</a>',
        'id'         => 'step1_main',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'step1_price',
                'type'     => 'text',
                'title'    => __( 'Price of Image', 'redux-framework-demo' ),
                'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),
                'desc'     => __( 'Field Description', 'redux-framework-demo' ),
                'default'  => '1200',
                'placeholder'  => 'Enter Price Here',
            ),
        )
    ) );
	
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Step 2', 'redux-framework-demo' ),
        'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="//docs.reduxframework.com/core/fields/textarea/" target="_blank">//docs.reduxframework.com/core/fields/textarea/</a>',
        'id'         => 'step2_main',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'step2_length',
                'type'     => 'multi_text',
                'title'    => __( 'length', 'redux-framework-demo' ),
                'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),
                'desc'     => __( 'Field Description', 'redux-framework-demo' ),
                'default'  => '1500',
            ), 
			array(
                'id'       => 'step2_length_price',
                'type'     => 'multi_text',
                'title'    => __( 'length Price', 'redux-framework-demo' ),
                'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),
                'desc'     => __( 'Price for each length', 'redux-framework-demo' ),
                'default'  => '1500',
            ),
        )
    ) );	
    
	Redux::setSection( $opt_name, array(
        'title'      => __( 'Step 3 (Texture)', 'redux-framework-demo' ),
        'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="//docs.reduxframework.com/core/fields/textarea/" target="_blank">//docs.reduxframework.com/core/fields/textarea/</a>',
        'id'         => 'step3_main',
        'subsection' => true,
        'fields'     => array(
			array(
                'id'         => 'step3_texture1',
                'type'       => 'media',
                'title'      => __( 'Texture Image 1', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step3_texture2',
                'type'       => 'media',
                'title'      => __( 'Texture Image 2', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step3_texture3',
                'type'       => 'media',
                'title'      => __( 'Texture Image 3', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step3_texture4',
                'type'       => 'media',
                'title'      => __( 'Texture Image 4', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step3_texture5',
                'type'       => 'media',
                'title'      => __( 'Texture Image 5', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step3_texture6',
                'type'       => 'media',
                'title'      => __( 'Texture Image 6', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step3_texture7',
                'type'       => 'media',
                'title'      => __( 'Texture Image 7', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step3_texture8',
                'type'       => 'media',
                'title'      => __( 'Texture Image 8', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),			
			array(
                'id'       => 'step3_texture_price',
                'type'     => 'multi_text',
                'title'    => __( 'Texture Price', 'redux-framework-demo' ),
                'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),
                'desc'     => __( 'Price for each Texture Type', 'redux-framework-demo' ),
                'default'  => '50',
				'placeholder'  => 'Enter Texture Price Here',
            ),
			array(
                'id'       => 'step3_texture_name',
                'type'     => 'multi_text',
                'title'    => __( 'Texture Name', 'redux-framework-demo' ),
                'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),
                'desc'     => __( 'Name for each Texture Type', 'redux-framework-demo' ),
                'default'  => '50',
				'placeholder'  => 'Enter Texture Name Here',
            ),
        )
    ) );	
	
	// Step 4 Color
	Redux::setSection( $opt_name, array(
        'title'      => __( 'Step 4 (Color)', 'redux-framework-demo' ),
        'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="//docs.reduxframework.com/core/fields/textarea/" target="_blank">//docs.reduxframework.com/core/fields/textarea/</a>',
        'id'         => 'step4_color_main',
        'subsection' => true,
        'fields'     => array(
			array(
                'id'         => 'step4_color0',
                'type'       => 'media',
                'title'      => __( 'Color Image 1', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step4_color1',
                'type'       => 'media',
                'title'      => __( 'Color Image 2', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step4_color2',
                'type'       => 'media',
                'title'      => __( 'Color Image 3', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step4_color3',
                'type'       => 'media',
                'title'      => __( 'Color Image 4', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step4_color4',
                'type'       => 'media',
                'title'      => __( 'Color Image 5', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step4_color5',
                'type'       => 'media',
                'title'      => __( 'Color Image 6', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step4_color6',
                'type'       => 'media',
                'title'      => __( 'Color Image 7', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step4_color7',
                'type'       => 'media',
                'title'      => __( 'Color Image 8', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),array(
                'id'         => 'step4_color8',
                'type'       => 'media',
                'title'      => __( 'Color Image 9', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),
			array(
                'id'         => 'step4_color9',
                'type'       => 'media',
                'title'      => __( 'Color Image 10', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),
			array(
                'id'         => 'step4_color10',
                'type'       => 'media',
                'title'      => __( 'Color Image 11', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),
			array(
                'id'         => 'step4_color11',
                'type'       => 'media',
                'title'      => __( 'Color Image 12', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),	
			array(
                'id'         => 'step4_color12',
                'type'       => 'media',
                'title'      => __( 'Color Image 13', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),	
			array(
                'id'         => 'step4_color13',
                'type'       => 'media',
                'title'      => __( 'Color Image 14', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),	
			array(
                'id'         => 'step4_color14',
                'type'       => 'media',
                'title'      => __( 'Color Image 15', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),	
			array(
                'id'         => 'step4_color15',
                'type'       => 'media',
                'title'      => __( 'Color Image 16', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),	
			array(
                'id'         => 'step4_color16',
                'type'       => 'media',
                'title'      => __( 'Color Image 17', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),	
			array(
                'id'         => 'step4_color17',
                'type'       => 'media',
                'title'      => __( 'Color Image 18', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),	
			array(
                'id'         => 'step4_color18',
                'type'       => 'media',
                'title'      => __( 'Color Image 19', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),	
			array(
                'id'         => 'step4_color19',
                'type'       => 'media',
                'title'      => __( 'Color Image 20', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),	
			array(
                'id'         => 'step4_color20',
                'type'       => 'media',
                'title'      => __( 'Color Image 21', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),	
			array(
                'id'         => 'step4_color21',
                'type'       => 'media',
                'title'      => __( 'Color Image 22', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),
			array(
                'id'         => 'step4_color22',
                'type'       => 'media',
                'title'      => __( 'Color Image 23', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),	
			array(
                'id'         => 'step4_color23',
                'type'       => 'media',
                'title'      => __( 'Color Image 24', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),
			array(
                'id'         => 'step4_color24',
                'type'       => 'media',
                'title'      => __( 'Color Image 25', 'redux-framework-demo' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'       => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            ),						
			array(
                'id'       => 'step4_color_price',
                'type'     => 'multi_text',
                'title'    => __( 'Color Price', 'redux-framework-demo' ),
                'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),
                'desc'     => __( 'Price for each Texture Type', 'redux-framework-demo' ),
                'default'  => '50',
				'placeholder'  => 'Enter Texture Price Here',
            ),
        )
    ) );

    /*
     * <--- END SECTIONS
     */
