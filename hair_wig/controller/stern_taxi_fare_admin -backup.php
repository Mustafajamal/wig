<?php

if ( ! defined( 'ABSPATH' ) )
	exit;

/**
 * Event admin
 */
class stern_taxi_fare_events_Admin {

	/**
	 * Constructor
	 */
	public function __construct() {

		add_action('admin_menu', array( $this,'register_stern_taxi_fare') );		
		//add_action('admin_menu', array( $this,'register_submenu_design') );		
		add_action('admin_menu', array( $this,'register_submenu_listAddress') );		
	}
	
	
/* 	public function create_post_type_car($cartype,$carfare,$carseat,$suitcases){
		$userID = 1;
		if(get_current_user_id()){
			$userID = get_current_user_id();
		}
		$post = array(
			'post_author' => $userID,
			'post_content' => '',
			'post_status' => 'publish',
			'post_title' => 'stern_taxi_car_type',
			'post_type' => 'stern_taxi_car_type',
		);

		$post_id = wp_insert_post($post);  
		update_post_meta($post_id, '_stern_taxi_car_type_cartype', $cartype);
		update_post_meta($post_id, '_stern_taxi_car_type_carfare', $carfare);
		update_post_meta($post_id, '_stern_taxi_car_type_carseat', $carseat);
		update_post_meta($post_id, '_stern_taxi_car_type_suitcases', $suitcases);
		
		
		return $post_id;
	}	 */

	public function register_stern_taxi_fare(){
		add_menu_page( 'Insurance Quote', 'Insurance Quote', 'manage_options', 'InsurancePage', array( $this,'menu_page_stern_taxi_fare'), plugins_url("img/", dirname(__FILE__)).'stern_taxi_fare.png', 6 ); 
	}
	
	
	function register_submenu_listAddress() {
		add_submenu_page( 'InsurancePage', __('Vehicle Info', 'stern_taxi_fare'),  __('Vehicle info', 'stern_taxi_fare'), 'manage_options', 'full-vehicle-info', array( $this,'my_custom_submenu_page_full_vehicle_info')  );
		add_submenu_page( 'InsurancePage', __('Coverage Data', 'stern_taxi_fare'),  __('Coverage Data', 'stern_taxi_fare'), 'manage_options', 'full-Coverage-data', array( $this,'my_custom_companies_page_frontend_data')  );

		add_submenu_page( 'InsurancePage', __('TPL Vehicle Info', 'stern_taxi_fare'),  __('TPL Vehicle Info', 'stern_taxi_fare'), 'manage_options', 'tpl-vehicle-data', array( $this,'my_custom_tpl_page_frontend_data')  );
		add_submenu_page( 'InsurancePage', __('TPL Coverage', 'stern_taxi_fare'),  __('TPL Coverage', 'stern_taxi_fare'), 'manage_options', 'tpl-coverage', array( $this,'my_custom_tpl_page_coverage_data')  );
	}	
	
	
	public function my_custom_submenu_page_callback_calendar(){
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['SternSaveSettingsCalendarTableSubmit']) ) {			
			updateOptionsSternTaxiFare();
		}
		
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['calendarSubmit']) ) {
			if( $_POST['dateTimeBegin']!=null && $_POST['dateTimeEnd']!=null  && $_POST['typeCalendar']!=null ) {
				
				$oCalendar = new calendar();
				$oCalendar->settypeIdCar(sanitize_text_field($_POST['typeIdCar']));
				$oCalendar->settypeCalendar(sanitize_text_field($_POST['typeCalendar']));
				$oCalendar->setisRepeat(sanitize_text_field($_POST['isRepeat']));

				
			//	$oCalendar->setdateEnd(sanitize_text_field($_POST['dateEnd']));
			//	$oCalendar->setdateBegin(sanitize_text_field($_POST['dateBegin']));
			
				$date1=date_create($_POST['dateBegin'] . " " . $_POST['dateTimeBegin']);
				$date1=date_format($date1,"Y/m/d g:i A");			
				$oCalendar->setdateTimeBegin(sanitize_text_field($date1));
				
				$date2=date_create($_POST['dateEnd'] . " " . $_POST['dateTimeEnd']);
				$date2=date_format($date2,"Y/m/d g:i A");				
				$oCalendar->setdateTimeEnd(sanitize_text_field($date2));
				
				
				if($date1<$date2) {
					$oCalendar->save();	
				} else {
					echo 'Date Begin is  greater than date End!';
				}
					
				
				
				
			}

			// Delete
			$args = array(
				'post_type' => 'stern_taxi_calendar',
				'posts_per_page' => 200,
			);

			$allPosts = get_posts( $args );			
			foreach ( $allPosts as $post ) {
			setup_postdata( $post );			
				if (isset($_POST['remove'.$post->ID])) {						
					if ($_POST['remove'.$post->ID] =='yes') {
						$oCalendar = new calendar($post->ID);
						$oCalendar->delete();
					}				
				}
			}
		}
		new templateCalendar("600px","300px",true);
	}	

	
		public function my_custom_submenu_page_full_vehicle_info(){
			
			//Upload csv Form		
			?>	
			<form id="featured_upload" method="post" action="#" enctype="multipart/form-data">
				<input type="file" name="my_image_upload_vinfo" id="my_image_upload_vinfo"  multiple="false" />
				<input type="hidden" name="post_id" id="post_id" value="0" />
				<?php wp_nonce_field( 'my_image_upload_vinfo', 'my_image_upload_vinfo' ); ?>
				<input id="submit_vinfo_upload" name="submit_vinfo_upload" type="submit" value="Upload CSV" />
			</form>
			
					<?php
		// uploaded data handler
			$post_id = 0;
			if ( 
			isset( $_POST['my_image_upload_vinfo'] ) 
			&& wp_verify_nonce( $_POST['my_image_upload_vinfo'], 'my_image_upload_vinfo' ))
			{
			// The nonce was valid and the user has the capabilities, it is safe to continue.

			// These files need to be included as dependencies when on the front end.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
			
			// Let WordPress handle the upload.
			// Remember, 'my_image_upload' is the name of our file input in our form above.
			$attachment_id_vinfo = media_handle_upload( 'my_image_upload_vinfo',$post_id);
			echo $attachment_id;
			update_option( 'attachment_id_vinfo', $attachment_id_vinfo,true );
			$id_uploaded_vinfo = get_option( 'attachment_id_vinfo' );
			$uploaded_vinfo = wp_get_attachment_url( $id_uploaded_vinfo );
			update_option( 'uplded_csvurl1', $uploaded_vinfo,true );
			echo $uploaded_vinfo;
			
			if ( is_wp_error( $attachment_id_vinfo ) ) {
				// There was an error uploading the image.
				echo "There was an error uploading the image.";
			} else {
				// The image was uploaded successfully!
				echo "The image was uploaded successfully!";
			}

			} else {
				// The security check failed, maybe show the user an error.
				//echo "The security check failed";
			}
			
			
			//Getting Data from uploaded CSV
			$uploaded_csvurlGet1 = get_option( 'uplded_csvurl1');
			$handle1 = fopen($uploaded_csvurlGet1, "r");
			for ($j = 0; $row_vinfo = fgetcsv($handle1 ); ++$j) {
				// Do something will $row array				
				$row_vinfoall[] = $row_vinfo;
			}			
			fclose($handle1); 
			
			update_option( 'frontend_dataVinfo', $row_vinfoall,true );
			?><pre><?php //print_r($row_vinfoall);?></pre><?php		
			//CSV uploaded data handler End.	

			?>
			<table id="show_formdata">
				<thead>
				<tr class="fd-head">
					<td>Vehicle Brand</td>	
					<td>Model Year</td>	
					<td>Model No/Sub Model</td>	
					<td>Add Min Value</td>	
					<td>Add Max Value</td>
					<td>Vehicle Category</td>	
				</tr>
				</thead>
				
				
				<?php
				//Shwo data Backend
				$csv_optionsvinfo = get_option( 'frontend_dataVinfo' );
				//$data_show = json_decode($csv_options, true); 
										
					foreach ( $csv_optionsvinfo as $get_parts ) {
												
						//for(k=0; k<=$data_show.length; k++){}
						?><tr class="fd-displayinner"><?php
						?> <td> <?php print_r($get_parts[0]); ?> </td> <?php				
						?> <td> <?php print_r($get_parts[1]); ?> </td> <?php				
						?> <td> <?php print_r($get_parts[2]); ?> </td> <?php				
						?> <td> <?php print_r($get_parts[3]); ?> </td> <?php				
						?> <td> <?php print_r($get_parts[4]); ?> </td> <?php				
						?> <td> <?php print_r($get_parts[5]); ?> </td> <?php					
						//}	
						?></tr><?php
						
					}
				?>
				
			</table>
			
			
			<style>
			#fd_rulestable td{
				font-weight: bold;
				border: 2px solid #e5e5e5;
				padding: 10px;					
			}
			#show_formdata .fd-head td{
				font-weight: bold;
				border: 2px solid #e5e5e5;
				padding: 10px;					
			}
			tr.fd-displayinner td {
				text-align: center;
				background: white;
			}
			
			</style>
			<?php
		}		
		
		
		//Step 3 Companies Data
		public function my_custom_companies_page_frontend_data(){
			
		//Upload csv Form		
		?>	
		<form id="featured_upload" method="post" action="#" enctype="multipart/form-data">
			<input type="file" name="my_image_upload" id="my_image_upload"  multiple="false" />
			<input type="hidden" name="post_id" id="post_id" value="0" />
			<?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>
			<input id="submit_my_image_upload" name="submit_my_image_upload" type="submit" value="Upload CSV" />
		</form>
		
		
		<?php
		// uploaded data handler
		$post_id = 0;
		if ( 
			isset( $_POST['my_image_upload_nonce'] ) 
			&& wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload' ))
		 {
			// The nonce was valid and the user has the capabilities, it is safe to continue.

			// These files need to be included as dependencies when on the front end.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
			
			// Let WordPress handle the upload.
			// Remember, 'my_image_upload' is the name of our file input in our form above.
			$attachment_id = media_handle_upload( 'my_image_upload',$post_id);
			echo $attachment_id;
			update_option( 'attachment_id_uploadedcsv', $attachment_id,true );
			$id_uploadedcsv_get = get_option( 'attachment_id_uploadedcsv' );
			$uploaded_csvurl = wp_get_attachment_url( $id_uploadedcsv_get );
			update_option( 'uplded_csvurl', $uploaded_csvurl,true );
			echo $uploaded_csvurl;
			
			if ( is_wp_error( $attachment_id ) ) {
				// There was an error uploading the image.
				echo "There was an error uploading the image.";
			} else {
				// The image was uploaded successfully!
				echo "The image was uploaded successfully!";
			}

			} else {
				// The security check failed, maybe show the user an error.
				//echo "The security check failed";
			}
			
			
			//Getting Data from uploaded CSV
			$uploaded_csvurlGet = get_option( 'uplded_csvurl');
			$handle = fopen($uploaded_csvurlGet, "r");
			for ($i = 0; $row = fgetcsv($handle ); ++$i) {
				// Do something will $row array				
				$row1[] = $row;
			}			
			fclose($handle); 			
			update_option( 'frontend_dataComp', $row1,true );
			?><pre><?php //print_r($row1);?></pre><?php		

			?>
			<!-- //Scroll table -->
			
			<div class="wrapper1">
	<div class="div1"></div>
</div>
			<div class="wrapper2">
				<div class="div2">
			
			<table id="show_formdata_comp">
				<thead>
				<!--
				<tr class="fd-head">
					<td>Company Name</td>	
					<td>Support Modified</td>	
					<td>Vehicle Category</td>	
					<td>Repair Condition</td>	
					<td>Min. Value</td>	
					<td>Max. Value</td>	
					<td>Rate</td>
					<td>Final Price(minimum)</td>
					<td>Driving license less than 1 Year</td>
					<td>Age less than 25 year</td>
					<td>Out Side UAE Coverage</td>
					<td>Personal Accident (Driver)</td>
					<td>Personal Accident Per Passengers</td>
					<td>Road Side Assistance</td>
					<td>Rant a Car</td>
					<td>Excess Amount</td>
				</tr> -->
				<tr class="fd-head">
					<td>Company Name</td>	
					<td>Class</td>	
					<td>Discounts</td>	
					<td>VAT</td>	
					<td>Excluded Vehicles List</td>	
					<td>Vehicle Category</td>	
					<td>Min. Value</td>
					<td>Max. Value</td>
					<td>Rate</td>
					<td>Minimum</td>
					<td>No Claim Certificate</td>
					<td>Self Declaration</td>
					<td>Repair Condition</td>
					<td>Personal Accident Cover for Driver</td>
					<td>Accident Cover for Passengers</td>
					<td>Rant a Car</td>
					<td>Own Damage Cover</td>
					<td>Out Side UAE Coverage</td>
					<td>IF UAE License Age less than 1 Year</td>
					<td>iF Age less than 25 year</td>
					<td>Road-side Assistance</td>
					<td>Third Party Limit</td>
					<td>Windscreen Damage</td>
					<td>Dent Repair</td>
					<td>Off-Road & Desert Recovery</td>
					<td>Emergency Medical Expenses</td>
					<td>Ambulance Cover</td>
					<td>Death or Bodily Injury</td>
					<td>Loss of Personal Belongings</td>
					<td>Fire and Theft Cover</td>
					<td>Valet Parking Theft</td>
					<td>Replacment of locks</td>
					<td>Natural Calamity Cover</td>
					<td>Car Registration / Renewal</td>
					<td>Modified / Non-GCC Support</td>
				</tr>
				</thead>
				
				
				<?php
				//Shwo data Backend
				$data_showComp = get_option( 'frontend_dataComp' );
				//$data_showComp = json_decode($csv_options, true); 

				 ?> <pre> <?php // print_r($data_showComp[0]); ?> </pre> <?php
				
					 foreach ( $data_showComp as $get_partsComp ) {
												
						//for($k=0; $k<=14; $k++){
						?><tr class="fd-displayinner-comp"><?php
						?> <td> <?php print_r($get_partsComp[0]); ?> </td> <?php				
 						?> <td> <?php print_r($get_partsComp[1]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[2]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[3]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[4]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[5]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[6]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[7]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[8]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[9]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[10]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[11]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[12]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[13]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[14]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[15]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[16]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[17]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[18]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[19]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[20]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[21]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[22]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[23]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[24]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[25]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[26]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[27]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[28]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[29]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[30]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[31]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[32]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[33]); ?> </td> <?php 				
						?> <td> <?php print_r($get_partsComp[34]); ?> </td> <?php 				
						//}	
						?></tr><?php						
					} 
				?>				
			</table>			
			</div>
			</div>			
			<!-- Scroll Table End -->
			
			<style>
			.wrapper1, .wrapper2 { width: 100%; overflow-x: scroll; overflow-y: hidden; }
.wrapper1 { height: 20px; }
.wrapper2 {}
.div1 { height: 20px; }
.div2 { overflow: none; }

			</style>	
			
			<script>
			jQuery(function(){
					jQuery('.wrapper1').on('scroll', function (e) {
						jQuery('.wrapper2').scrollLeft(jQuery('.wrapper1').scrollLeft());
					}); 
					jQuery('.wrapper2').on('scroll', function (e) {
						jQuery('.wrapper1').scrollLeft(jQuery('.wrapper2').scrollLeft());
					});
				});
				jQuery(window).on('load', function (e) {
					jQuery('.div1').width(jQuery('table').width());
					jQuery('.div2').width(jQuery('table').width());
				});

			</script>
			
			
			<!-- Scroll Exp-->
			
			
			
			
			
			<style>
			#fd_companiestable td{
				font-weight: bold;
				border: 2px solid #e5e5e5;
				padding: 10px;
					
			}

			#show_formdata_comp .fd-head td{
				font-weight: bold;
				border: 2px solid #e5e5e5;
				padding: 10px;
					
			}
			tr.fd-displayinner-comp td {
				text-align: center;
				background: white;
			}
			div#cinfo-tabstp2cont {
				width: 100% !important;
				display: block !important;
				overflow: scroll !important;
				scroll-behavior: smooth !important;
			}
			</style>
			<!--
			<h2>Upload Data Here</h2>
			<table id="fd_companiestable">
			<tr>
					<td>Company Name</td>	
					<td>Support Modified</td>	
					<td>Vehicle Category</td>	
					<td>Repair Condition</td>	
					<td>Min. Value</td>	
					<td>Max. Value</td>	
					<td>Rate</td>
					<td>Final Price(minimum)</td>
					<td>Driving license less than 1 Year</td>
					<td>Age less than 25 year</td>
					<td>Out Side UAE Coverage</td>
					<td>Personal Accident (Driver)</td>
					<td>Personal Accident Per Passengers</td>
					<td>Road Side Assistance</td>
					<td>Rant a Car</td>
					<td>Excess Amount</td>
			<tr/>
			</table>
			<form method="post">
			<textarea cols="201" rows="5" name="fdCompanies" id="fdCompanies"></textarea><br/>
				<input type="submit" id="fdCompaniesSubmit" value="Go" class="button-primary" name="fdCompaniesSubmit" />
			</form>
			-->
			<?php
		}		
		
		//TPL Vehicle Info
		public function my_custom_tpl_page_frontend_data(){
			
		//Upload csv Form		
		?>	
		<form id="featured_upload" method="post" action="#" enctype="multipart/form-data">
			<input type="file" name="my_image_upload" id="my_image_upload"  multiple="false" />
			<input type="hidden" name="post_id" id="post_id" value="0" />
			<?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>
			<input id="submit_my_image_upload" name="submit_my_image_upload" type="submit" value="Upload CSV" />
		</form>
		
		
		<?php
		// uploaded data handler TPL
		$post_id = 0;
		if ( 
			isset( $_POST['my_image_upload_nonce'] ) 
			&& wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload' ))
		 {
			// The nonce was valid and the user has the capabilities, it is safe to continue.

			// These files need to be included as dependencies when on the front end.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
			
			// Let WordPress handle the upload.
			// Remember, 'my_image_upload' is the name of our file input in our form above.
			$attachment_id = media_handle_upload( 'my_image_upload',$post_id);
			echo $attachment_id;
			update_option( 'attachment_id_uploadedcsv3', $attachment_id,true );
			$id_uploadedcsv_get3 = get_option( 'attachment_id_uploadedcsv3' );
			$uploaded_csvurl3 = wp_get_attachment_url( $id_uploadedcsv_get3 );
			update_option( 'uplded_csvurl3', $uploaded_csvurl3,true );
			echo $uploaded_csvurl3;
			
			if ( is_wp_error( $attachment_id ) ) {
				// There was an error uploading the image.
				echo "There was an error uploading the image.";
			} else {
				// The image was uploaded successfully!
				echo "The image was uploaded successfully!";
			}

			} else {
				// The security check failed, maybe show the user an error.
				//echo "The security check failed";
			}
			
			
			//Getting Data from uploaded CSV
			$uploaded_csvurlGet = get_option( 'uplded_csvurl3');
			$handle = fopen($uploaded_csvurlGet, "r");
			for ($i = 0; $row = fgetcsv($handle ); ++$i) {
				// Do something will $row array				
				$row1[] = $row;
			}			
			fclose($handle); 			
			update_option( 'vehicle_dataTpl', $row1,true );
			?><pre><?php //print_r($row1);?></pre><?php		

			?>
			<!-- //Scroll table -->
			
			<div class="wrapper1">
				<div class="div1"></div>
			</div>
			<div class="wrapper2">
				<div class="div2">
			
			<table id="show_formdata_comp">
				<thead>
				<tr class="fd-head">
					<td>Vehicle Brand</td>	
					<td>Model Year</td>	
					<td>Model No / Sub Model</td>	
					<td>Vehicle Category</td>							
				</tr>
				</thead>				
				
				<?php
				//Shwo data Backend
				$data_showTpl1 = get_option( 'vehicle_dataTpl' );
				//$data_showComp = json_decode($csv_options, true); 

				 ?> <pre> <?php // print_r($data_showComp[0]); ?> </pre> <?php
				
					 foreach ( $data_showTpl1 as $get_partsComp ) {
												
						//for($k=0; $k<=14; $k++){
						?><tr class="fd-displayinner-comp"><?php
						?> <td> <?php print_r($get_partsComp[0]); ?> </td> <?php				
 						?> <td> <?php print_r($get_partsComp[1]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[2]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[3]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[4]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[5]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[6]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[7]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[8]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[9]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[10]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[11]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[12]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[13]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[14]); ?> </td> <?php									
						//}	
						?></tr><?php						
					} 
				?>				
			</table>			
			</div>
			</div>						
			<!-- Scroll Exp-->			
			
			<style>
			#fd_companiestable td{
				font-weight: bold;
				border: 2px solid #e5e5e5;
				padding: 10px;
					
			}

			#show_formdata_comp .fd-head td{
				font-weight: bold;
				border: 2px solid #e5e5e5;
				padding: 10px;
					
			}
			tr.fd-displayinner-comp td {
				text-align: center;
				background: white;
			}
			div#cinfo-tabstp2cont {
				width: 100% !important;
				display: block !important;
				overflow: scroll !important;
				scroll-behavior: smooth !important;
			}
			</style>
			<?php
		}
		
		//TPL Coverage Data
		public function my_custom_tpl_page_coverage_data(){
			
		//Upload csv Form		
		?>	
		<form id="featured_upload" method="post" action="#" enctype="multipart/form-data">
			<input type="file" name="my_image_upload4" id="my_image_upload4"  multiple="false" />
			<input type="hidden" name="post_id" id="post_id" value="0" />
			<?php wp_nonce_field( 'my_image_upload4', 'my_image_upload_nonce' ); ?>
			<input id="submit_my_image_upload" name="submit_my_image_upload" type="submit" value="Upload CSV" />
		</form>
		
		
		<?php
		// uploaded data handler TPL
		$post_id = 0;
		if ( 
			isset( $_POST['my_image_upload_nonce'] ) 
			&& wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload4' ))
		 {
			// The nonce was valid and the user has the capabilities, it is safe to continue.

			// These files need to be included as dependencies when on the front end.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
			
			// Let WordPress handle the upload.
			// Remember, 'my_image_upload4' is the name of our file input in our form above.
			$attachment_id4 = media_handle_upload( 'my_image_upload4',$post_id);
			echo $attachment_id4;
			update_option( 'attachment_id_uploadedcsv4', $attachment_id4,true );
			$id_uploadedcsv_get4 = get_option( 'attachment_id_uploadedcsv4' );
			$uploaded_csvurl4 = wp_get_attachment_url( $id_uploadedcsv_get4 );
			update_option( 'uplded_csvurl4', $uploaded_csvurl4,true );
			echo $uploaded_csvurl4;
			
			if ( is_wp_error( $attachment_id4 ) ) {
				// There was an error uploading the image.
				echo "There was an error uploading the image.";
			} else {
				// The image was uploaded successfully!
				echo "The image was uploaded successfully!";
			}

			} else {
				// The security check failed, maybe show the user an error.
				//echo "The security check failed";
			}
			
			
			//Getting Data from uploaded CSV
			$uploaded_csvurlGet4 = get_option( 'uplded_csvurl4');
			$handle4 = fopen($uploaded_csvurlGet4, "r");
			for ($i = 0; $row = fgetcsv($handle4); ++$i) {
				// Do something will $row array				
				$row1[] = $row;
			}			
			fclose($handle4); 			
			update_option( 'frontend_dataTpl', $row1,true );
			?><pre><?php //print_r($row1);?></pre><?php		

			?>
			<!-- //Scroll table -->
			
			<div class="wrapper1">
				<div class="div1"></div>
			</div>
			<div class="wrapper2">
				<div class="div2">
			
			<table id="show_formdata_comp">
				<thead>
				<tr class="fd-head">
					<td>Company Name</td>	
					<td>Class</td>	
					<td>Discounts</td>	
					<td>Excluded Vehicles List</td>	
					<td>Vehicle Category</td>	
					<td>Policy Type</td>	
					<td>Cylinders / Capacity</td>	
					<td>Q. Price</td>	
					<td>VAT. Cost</td>	
					<td>Personal Accident Cover for Driver</td>	
					<td>Accident Cover for Passengers</td>	
					<td>Third Party Limit</td>	
					<td>Third Party Death or Bodily</td>	
					<td>Ambulance Cover</td>	
					<td>Road-side Assistance</td>	
				</tr>
				</thead>
				
				
				<?php
				//Shwo data Backend
				$data_showTpl = get_option( 'frontend_dataTpl' );
				//$data_showComp = json_decode($csv_options, true); 

				 ?> <pre> <?php // print_r($data_showComp[0]); ?> </pre> <?php
				
					 foreach ( $data_showTpl as $get_partsComp ) {
												
						//for($k=0; $k<=14; $k++){
						?><tr class="fd-displayinner-comp"><?php
						?> <td> <?php print_r($get_partsComp[0]); ?> </td> <?php				
 						?> <td> <?php print_r($get_partsComp[1]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[2]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[3]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[4]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[5]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[6]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[7]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[8]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[9]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[10]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[11]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[12]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[13]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[14]); ?> </td> <?php									
						//}	
						?></tr><?php						
					} 
				?>				
			</table>			
			</div>
			</div>						
			<!-- Scroll Exp-->			
			
			<style>
			#fd_companiestable td{
				font-weight: bold;
				border: 2px solid #e5e5e5;
				padding: 10px;
					
			}

			#show_formdata_comp .fd-head td{
				font-weight: bold;
				border: 2px solid #e5e5e5;
				padding: 10px;
					
			}
			tr.fd-displayinner-comp td {
				text-align: center;
				background: white;
			}
			div#cinfo-tabstp2cont {
				width: 100% !important;
				display: block !important;
				overflow: scroll !important;
				scroll-behavior: smooth !important;
			}
			</style>
			<?php
		}
		
		
		
	public function my_custom_submenu_page_callback_list_addresses(){
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['SettingsTemplateListAddressSubmit']) ) {
			updateOptionsSternTaxiFare();			
		}			
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['listAddressSubmit']) ) {
			if( $_POST['typeListAddress']!=null && $_POST['address']!=null  ) {
				$oListAddress = new listAddress();
				$oListAddress->setisActive(sanitize_text_field($_POST['isActive']));
				$oListAddress->settypeListAddress(sanitize_text_field($_POST['typeListAddress']));				
				$oListAddress->setaddress(sanitize_text_field($_POST['address']));
				$oListAddress->save();	
			} else {
				$args = array(
				'post_type' => 'stern_listAddress',
				'nopaging' => true,
				);

				$allPosts = get_posts( $args );			
				foreach ( $allPosts as $post ) {
					setup_postdata( $post );
					$oListAddress = new listAddress($post->ID);			
					if (isset($_POST['remove'.$post->ID])) {						
						$oListAddress->delete();
					}
					if (isset($_POST['isActive'.$post->ID])) {									
						$oListAddress->setisActive("true");
						$oListAddress->save();
					} else {
						$oListAddress->setisActive("false");
						$oListAddress->save();
					}		
				
				}
			}
			
		}		
		new templateListAddress("600px","300px",true);
	}
		
	public function my_custom_submenu_page_callback_rule(){
		
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['SettingsPricingRulesSubmit']) ) {
			updateOptionsSternTaxiFare();			
		}
	
		
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['ruleSubmit']) ) {
			if( $_POST['price']!=null && $_POST['nameRule']!=null  ) {				
				$oRule = new rule();	
				$oRule->setisActive(sanitize_text_field($_POST['isActive']));
				$oRule->setnameRule(sanitize_text_field($_POST['nameRule']));				
				$oRule->settypeSource(sanitize_text_field($_POST['typeSource']));
				$oRule->settypeSourceValue(sanitize_text_field($_POST['typeSourceValue']));
				$oRule->settypeDestination(sanitize_text_field($_POST['typeDestination']));
				$oRule->settypeDestinationValue(sanitize_text_field($_POST['typeDestinationValue']));
				$oRule->settypeIdCar(sanitize_text_field($_POST['typeIdCar']));
				$oRule->setprice(sanitize_text_field($_POST['price']));	
				$oRule->save();				
			}

			// Delete
			
		if(isset($_GET["paged"])) {
			$paged = $_GET["paged"];
		} else {
			$paged = 1;
		}
			//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			
		if(get_option('stern_taxi_fare_nb_post_to_show')=="") {
			$posts_per_page = 10;
		} else {
			$posts_per_page = get_option('stern_taxi_fare_nb_post_to_show');
		}
		$query = getQueryRule($paged, $posts_per_page );			
			
			while ( $query->have_posts() ) : $query->the_post(); 
				$oRule = new rule(get_the_ID());
				if (isset($_POST['remove'.get_the_ID()])) {				
					$oRule->delete();
					
				} 
				
				if (isset($_POST['isActive'.get_the_ID()])) {						
					$oRule->setisActive("true");
					$oRule->save();
					
				} else {
					$oRule->setisActive("false");
					$oRule->save();
					
				}
			endwhile;
						
			
			/*
			$args = array(
			'post_type' => 'stern_taxi_rule',
			'nopaging' => true,
			);

			$allPosts = get_posts( $args );			
			foreach ( $allPosts as $post ) {
				setup_postdata( $post );
				$oRule = new rule($post->ID);			
				if (isset($_POST['remove'.$post->ID])) {						
					$oRule->delete();
					
				} 
				
				if (isset($_POST['isActive'.$post->ID])) {						
					$oRule->setisActive("true");
					$oRule->save();
					
				} else {
					$oRule->setisActive("false");
					$oRule->save();
					
				}
				
				
			
			}
			*/
		}
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['deleteAllRules']) ) {
			
			$args = array(
			'post_type' => 'stern_taxi_rule',
			'nopaging' => true,
			);

			$allPosts = get_posts( $args );			
			foreach ( $allPosts as $post ) {
				setup_postdata( $post );
				$oRule = new rule($post->ID);			
										
				$oRule->delete();			
			}			
			
		}
	//	var_dump( $bulkData);
	//	var_dump($parts);
		new templateRule("600px","300px",true);
	}

	//true	lille-marseille2	city	Lille, France	city	Marseille, France	All	9
			
	public function my_custom_submenu_page_callback_design(){
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['SternSaveSettings']) ) 
        {		
			updateOptionsSternTaxiFare();
		}
		
		new design("600px","300px",true);
	}
	

	public function menu_page_stern_taxi_fare(){
		
		
		?>
		
		<div id="content">
		 <h2>Enter this shortcode to view form [insurance-qoute-system]</h2>
		 <h2>Enter this shortcode to view TPL form [insurance-qoute-system-tpl]</h2>		
		</div>
		
		<?php
		
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['SternSaveSettings']) ) 
        {		
			updateOptionsSternTaxiFare();
			saveVersion();
			sendInfosDebug();

		}
		
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['initVal']) ) 
        {

			
			$beginNameOption = "stern_taxi_fare%";
			global $wpdb;
			$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->options WHERE option_name like %s",$beginNameOption));
		
						
					
		}
		if (($_SERVER['REQUEST_METHOD'] == 'POST') && isset($_POST['createProduct']) ) 
        {
			createProductAndSaveId();			
			
		}		
		
		
		new settings("600px","300px",true);
	}




}

new stern_taxi_fare_events_Admin();