<?php
/**
 * Admin new order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/admin-new-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author WooThemes
 * @package WooCommerce/Templates/Emails/HTML
 * @version 2.5.0
 */

 if ( ! defined( 'ABSPATH' ) ) {
 	exit;
 }

 /**
  * @hooked WC_Emails::email_header() Output the email header
  */
 do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

 <p><?php printf( __( 'You have received an order from %s. The order is as follows:', 'woocommerce' ), $order->get_formatted_billing_full_name() ); ?></p>

 	<?php
	$cart_element = WC()->cart;
	if($cart_element){
		foreach( WC()->cart->get_cart() as $cart_item ){
			$product_id = $cart_item['product_id'];
		}
	}
	
	//Get data from PHP Sessions
	$price_image =  get_option('price_image');
	$price_length = get_option('price_length');
	$price_texture = get_option('price_texture');
	$price_color = get_option('price_color');
	$custom_wig = get_option('custom_wig');
	$color_name = get_option('color_name');
	$sel_ImageCode1 = get_option('sel_ImageCode');
	$sel_lengthVal = get_option('sel_lengthVal');
	$sel_texture_name = get_option('sel_texture_name');	
	$wig_product_id = get_option('stern_taxi_fare_product_id_wc'); 
	
	session_start();  	
	$wig_imgCode = $_SESSION["wig_imgCode"]; 
	
	//echo $wig_imgCode;	 
	//$product_id22 = $order->get_item_meta($item_id, '_product_id', true); // product ID
	$items = $order->get_items();
	//$test = wc_get_order( $order_id );
	
	foreach ( $items as $item ) {
		$product_id_email = $item->get_product_id();
	}
			
	//echo $product_id22222;
	//echo $test;
	?></pre><?php //print_r($order); ?></pre><?
	$wig_product_id = get_option('wig_prod_id');
	
	//if($product_id_email == $wig_product_id){
	?>
	
	<h3 style="text-align:center;">Custom Wig Image Theme1</h3>

	<?php //echo $sel_ImageCode1; ?>

	<div style="display:block;text-align:center;"><img src="<?php //echo $sel_ImageCode1;?>" style="width:100px;height:auto;" ></div>
	
	<h3 style="text-align:center;">Selected Items</h3>
	
	<div id="bought_things" class="rightsidebar">								
		<div style="display:inline-block;" class="base_price_block ">	
		<p>Custom Wig: <span style="color:red;font-weight:bold;"><?php echo $price_image;?></span></p>
		<p>Length: <span style="color:red;font-weight:bold;"> <?php echo $sel_lengthVal;?></span><span style="color:red;font-weight:bold;"><?php echo "$".$price_length;?></span></p>
		<p>Texture: <span style="color:red;font-weight:bold;">   <?php echo $sel_texture_name;?></span><span style="color:red;font-weight:bold;"><?php echo "$".$price_texture;?></span></p>				
		<p>Color: <span style="color:red;font-weight:bold;"><?php echo $color_name;?></span><span style="color:red;font-weight:bold;"><?php echo "$".$price_color;?></span></p>				
		</div>
	</div>
	
	<?php //} ?>

 
 
	
<h3 style="align:center">Order Summary</h3>

<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
	<thead>
		<tr>
			<th class="td" scope="col" style="text-align:<?php echo $text_align; ?>;"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:<?php echo $text_align; ?>;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:<?php echo $text_align; ?>;"><?php _e( 'Price', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo wc_get_email_order_items( $order, array(
			'show_sku'      => $sent_to_admin,
			'show_image'    => true,
			'image_size'    => array( 32, 32 ),
			'plain_text'    => $plain_text,
			'sent_to_admin' => $sent_to_admin,
		) ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr>
						<th class="td" scope="row" colspan="2" style="text-align:<?php echo $text_align; ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo $total['label']; ?></th>
						<td class="td" style="text-align:<?php echo $text_align; ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo $total['value']; ?></td>
					</tr><?php
				}
			}
		?>
	</tfoot>
</table>



 <?php

 /**
  * @hooked WC_Emails::order_details() Shows the order details table.
  * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
  * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
  * @since 2.5.0
  */
 /**
  * @hooked WC_Emails::order_meta() Shows order meta data.
  */
 /**
  * @hooked WC_Emails::customer_details() Shows customer details
  * @hooked WC_Emails::email_address() Shows email address
  */
 do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );
 do_action( 'woocommerce_email_email_address', $order, $sent_to_admin, $plain_text, $email );
 /**
  * @hooked WC_Emails::email_footer() Output the email footer
  */
 do_action( 'woocommerce_email_footer', $email );
