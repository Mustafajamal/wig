<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$text_align = is_rtl() ? 'right' : 'left';

do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>

<?php if ( ! $sent_to_admin ) : ?>
	<h2><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></h2>
<?php else : ?>
	<h2><a class="link" href="<?php echo esc_url( admin_url( 'post.php?post=' . $order->get_id() . '&action=edit' ) ); ?>"><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></a> (<?php printf( '<time datetime="%s">%s</time>', $order->get_date_created()->format( 'c' ), wc_format_datetime( $order->get_date_created() ) ); ?>)</h2>
<?php endif; ?>
	
	<?php
	$cart_element = WC()->cart;
	if($cart_element){
		foreach( WC()->cart->get_cart() as $cart_item ){
			$product_title = $cart_item['product_title'];
		}
	}
	
	//Get data from PHP Sessions
	$price_image =  get_option('price_image');
	$price_length = get_option('price_length');
	$price_texture = get_option('price_texture');
	$price_color = get_option('price_color');
	$custom_wig = get_option('custom_wig');
	$color_name = get_option('color_name');
	$sel_ImageCode1 = get_option('sel_ImageCode');
	$sel_lengthVal = get_option('sel_lengthVal');
	$sel_texture_name = get_option('sel_texture_name');
	
	$wig_product_id = get_option('stern_taxi_fare_product_id_wc'); 
	
	?>		
	<!-- Sidebar right -->
		<script>
	var get_image64bit1 = localStorage.getItem("selected_imageCode");
	//jQuery("#selected_wig_img").attr('src', get_image64bit1);
	</script>
	<h3 style="text-align:center;">Custom Wig Image </h3>	
	<?php session_start();  	
	$wig_imgCode = $_SESSION["wig_imgCode"]; 
	//echo $wig_imgCode;
	?>
	<?php if($product_title == 'Hair Wig'){
	?>
	
	<div style="display:block;text-align:center;"><img id="selected_wig_img" src="<?php echo $wig_imgCode;?>" style="width:100px;height:auto;" ></div>
	
	<h3 style="text-align:center;">Selected Items</h3>
	
	<div id="bought_things" class="rightsidebar">								
		<div style="display:inline-block;" class="base_price_block ">	
		<p>Custom Wig: <span style="color:red;font-weight:bold;"><?php echo "$".$price_image;?></span></p>
		<p>Length: <span style="color:red;font-weight:bold;"> <?php echo $sel_lengthVal;?></span>  --  <span style="color:red;font-weight:bold;"><?php echo "$".$price_length;?></span></p>
		<p>Texture: <span style="color:red;font-weight:bold;">   <?php echo $sel_texture_name;?></span><span style="color:red;font-weight:bold;">     <?php echo "$".$price_texture;?></span></p>				
		<p>Color: <span style="color:red;font-weight:bold;"><?php echo $color_name;?></span><span style="color:red;font-weight:bold;">     <?php echo "$".$price_color;?></span></p>				
		</div>
	</div>
	<?php } ?>
<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
	<thead>
		<tr>
			<th class="td" scope="col" style="text-align:<?php echo $text_align; ?>;"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:<?php echo $text_align; ?>;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:<?php echo $text_align; ?>;"><?php _e( 'Price', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo wc_get_email_order_items( $order, array(
			'show_sku'      => $sent_to_admin,
			'show_image'    => true,
			'image_size'    => array( 32, 32 ),
			'plain_text'    => $plain_text,
			'sent_to_admin' => $sent_to_admin,
		) ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr>
						<th class="td" scope="row" colspan="2" style="text-align:<?php echo $text_align; ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo $total['label']; ?></th>
						<td class="td" style="text-align:<?php echo $text_align; ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo $total['value']; ?></td>
					</tr><?php
				}
			}
		?>
	</tfoot>
</table>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
