<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
foreach( WC()->cart->get_cart() as $cart_item ){
		$product_id = $cart_item['product_id'];
	}
	
	//Get data from PHP Sessions
	$price_image =  get_option('price_image');
	$price_length = get_option('price_length');
	$price_texture = get_option('price_texture');
	$price_color = get_option('price_color');
	$custom_wig = get_option('custom_wig');
	$color_name = get_option('color_name');
	$sel_ImageCode1 = get_option('sel_ImageCode');
	$sel_lengthVal = get_option('sel_lengthVal');
	$sel_texture_name = get_option('sel_texture_name');
	
	$wig_product_id = get_option('stern_taxi_fare_product_id_wc');	?>		
	<!-- Sidebar right -->
	<script>
	var get_image64bit1 = localStorage.getItem("selected_imageCode");
	//jQuery("#selected_wig_img").attr('src', get_image64bit1);
	</script>
	<!-- <h3 style="text-align:center;">Custom Wig Image</h3>	-->
	<?php session_start();  	
	$wig_imgCode = $_SESSION["wig_imgCode"]; 
	//echo $wig_imgCode;
	?>

	<!--
	<div style="display:block;text-align:center;"><img id="selected_wig_img" src="<?php //echo $wig_imgCode;?>" style="width:100px;height:auto;" ></div>
	
	<h3 style="text-align:center;">Selected Items</h3>
	
	<div id="bought_things" class="rightsidebar">								
		<div style="width:40%;float:left;display:inline-block;" class="base_price_block ">								
		
		<p>Custom Wig:</p>
		<p>Length: <span style="color:red;font-weight:bold;"><?php //echo $sel_lengthVal;?></span></p>
		<p>Texture: <span style="color:red;font-weight:bold;"><?php// echo $sel_texture_name;?></span></p>				
		<p>Color: <span style="color:red;font-weight:bold;"><?php //echo $color_name;?></span></p>				
		</div>
		<div style="width:40%;float:right;display:inline-block;" class="base_price_block">								
		<p style="color:red;font-weight:bold;"><?php //echo "$".$price_image;?></p>
		<p style="color:red;font-weight:bold;"><?php //echo "$".$price_length;?></p>
		<p style="color:red;font-weight:bold;"><?php //echo "$".$price_texture;?></p>						
		<p style="color:red;font-weight:bold;"><?php //echo "$".$price_color;?></p>						
		</div>
			
	</div>
	-->
	
<table class="shop_table woocommerce-checkout-review-order-table">
	<thead>
		<tr>
			<th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th class="product-total"><?php _e( 'Total', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
			do_action( 'woocommerce_review_order_before_cart_contents' );

			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					?>
					<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
						<td class="product-name">
							<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;'; ?>
							<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times; %s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); ?>
							<?php echo WC()->cart->get_item_data( $cart_item ); ?>
						</td>
						<td class="product-total">
							<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
						</td>
					</tr>
					<?php
				}
			}

			do_action( 'woocommerce_review_order_after_cart_contents' );
		?>
	</tbody>
	<tfoot>

		<tr class="cart-subtotal">
			<th><?php _e( 'Subtotal', 'woocommerce' ); ?></th>
			<td><?php wc_cart_totals_subtotal_html(); ?></td>
		</tr>

		<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
			<tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
				<th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
				<td><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
			</tr>
		<?php endforeach; ?>

		<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

			<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

			<?php wc_cart_totals_shipping_html(); ?>

			<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>
		<?php endif; ?>

		<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
			<tr class="fee">
				<th><?php echo esc_html( $fee->name ); ?></th>
				<td><?php wc_cart_totals_fee_html( $fee ); ?></td>
			</tr>
		<?php endforeach; ?>

		<?php if ( wc_tax_enabled() && 'excl' === WC()->cart->tax_display_cart ) : ?>
			<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
				<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
					<tr class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
						<th><?php echo esc_html( $tax->label ); ?></th>
						<td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
					</tr>
				<?php endforeach; ?>
			<?php else : ?>
				<tr class="tax-total">
					<th><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></th>
					<td><?php wc_cart_totals_taxes_total_html(); ?></td>
				</tr>
			<?php endif; ?>
		<?php endif; ?>

		<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

		<tr class="order-total">
			<th><?php _e( 'Total', 'woocommerce' ); ?></th>
			<td><?php wc_cart_totals_order_total_html(); ?></td>
		</tr>

		<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

	</tfoot>
</table>
