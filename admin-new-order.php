<?php
/**
 * Admin new order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/admin-new-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author WooThemes
 * @package WooCommerce/Templates/Emails/HTML
 * @version 2.5.0
 */

 if ( ! defined( 'ABSPATH' ) ) {
 	exit;
 }

 /**
  * @hooked WC_Emails::email_header() Output the email header
  */
 do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

 <p><?php printf( __( 'You have received an order from %s. The order is as follows:', 'woocommerce' ), $order->get_formatted_billing_full_name() ); ?></p>

 	<?php
	$cart_element = WC()->cart;
	if($cart_element){
		foreach( WC()->cart->get_cart() as $cart_item ){
			$product_id = $cart_item['product_id'];
		}
	}
	
	//Get data from PHP Sessions
	$price_image =  get_option('price_image');
	$price_length = get_option('price_length');
	$price_texture = get_option('price_texture');
	$price_color = get_option('price_color');
	$custom_wig = get_option('custom_wig');
	$color_name = get_option('color_name');
	$sel_ImageCode1 = get_option('sel_ImageCode');
	$sel_lengthVal = get_option('sel_lengthVal');
	$sel_texture_name = get_option('sel_texture_name');	
	$wig_product_id = get_option('stern_taxi_fare_product_id_wc'); 
	
	session_start();  	
	$wig_imgCode = $_SESSION["wig_imgCode"]; 
	
	//echo $wig_imgCode;	 
	//$product_id22 = $order->get_item_meta($item_id, '_product_id', true); // product ID
	$items = $order->get_items();
	//$test = wc_get_order( $order_id );
	
	foreach ( $items as $item ) {
		$product_id_email = $item->get_product_id();
	}
			
	//echo $product_id22222;
	//echo $test;
	?></pre><?php //print_r($order); ?></pre><?
	$wig_product_id = get_option('wig_prod_id');
	
	//if($product_id_email == $wig_product_id){
	?>
	
	<h3 style="text-align:center;">Custom Wig Image Theme1</h3>

	<?php //echo $sel_ImageCode1; ?>

	<div style="display:block;text-align:center;"><img src="sel_imgCode..data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAABjCAYAAADeg0+zAAAgAElEQVR4Xu1dB3xV1f3/nXPH28nLHpAQIKyEvQSkDAEnivpnuKp1K9ZV22qrFaxVW+uqdVWR1i1QraCIKAoyVfYmJEAGScgeb951zv9z7n3v5WVPprmOvPfuPfOe7/nt30HQzovSUru37OgkufLAhar76Dgql/dHVIrCHAcYY2B/ARAARsD+AYSN7/rn4G/se+C5er+x33GD54JlcaC+wPdQfcbzjdtg7QbartdG2G+IA+BEQBQBBQwo/DkEQPU22L+sDLvYj2G/UQqAaOA31gf2VQOgFKj+jd0j+nd9Cth99rxoAmSNNp7wVAFVZOMZ9i/7qz/I2mSfjbGxZkN9CHwKtRFojT0fbBlR1nujb5S1aVRuVIcpUJW1afQ1eM8oG+hv4K/xW6Bsm38zxl2/bF0d+u96u0Zb9dsN3gv8TghQSoBoBDRNA0KI8Z+mAdXYZ/abpt/X/6ph9wEpnBiVL0am7bTFDVkbmTL2a3Nkek57lnxo2lsr5PHkJvlyv5smVeycTLXaTIwgHvF8FMebMEaCxAmizEARAghiyyqw4IMLrKmFytZ3k0DSV1Pdwg8BKrBQwwEXBGBz4GoOrKoMVPUA5RiUEJAgnjUEoLE1ygFGvN4/fYHqL1UfmPEboYAwASAYKEMXgxl7oexRhjrA+ggo+4A5ZFSIMQgiwbY4P9UoUF+FCVQZszdLNVaYEAB2Q28vuFpY/UBYRcbw9acw1peY/hvRGwwCihVXjOJY74s+HtYFohDACgWw2YHyJgNEOlKNfus1soYCi7gO6OyTpv8eBGE4uAwws94EywbrCw0hMH+a0Y7eBgOIUWPwc3gdeg2sXwxEbOFTAwjsu6aqeh0MLJT9rhLQdLAY94mqYaL6zJoii5om1xKNlgMWj1ui0jcm9Lv0G2f6RdtbW/PBldbic5QWWUt3f/ILf9nOiwHBaIxof04QyzEn5iAOF2PeVIqxUI14XuYwpuz9629FX3SMSrAruAOzz/qNBr8F7wee5+pKGpQm7Pl6ZZuon1Ex/fngPfaxQf3BJxQvaO5iRMxGl1SgSO8+e88+9pkHjjPptWkMGERDnACgEcJoDxDCnqFIf14njxSBJnOE6HjDwGpDlAfMixhzJuBEAbBZBbO9BqwJlUAoBX+5E/w1UVTz81RTJFA1GUBRgCLVgAEhGAFRAQNGXIA8YUYfjHWGdXAbpIytX4wDK14CSdVAsGBGwwAUACwAED+lvA8A4pIomOxh754VDgAk+BE0Y2j6z8FPoV8CgGn+nlENw3uwEvYhvDwbXvi9wPOhnmhssgPNs2c10HRKwbqj6PWyPYV90CmMTl3Yj+xRFWuq304UKY4QEq+pWpqq+vsQotYAcDtMlvhvkobf9l1034n5YZPQ6GOLFKTk6JoEJW/VLzTw36h5Sy5AGHJ5kyPHHNV/kyV+xE6bLe0IRA4oZ8upbrZaau4MvMd2/oWBfi0I9Q/BE4AgKcgYjQKI8iPweDCkAUCZasybX8EQqxhIdDsRWFXsrpQEbDNzCAQOgYNDZpsJW2OtYHGagYsigiO6BpKHV4BjZpW+3dZsjIQTq+MVr8cBcgmlLo9M/VV+Ch6JgkuzkGoVMK+5KhF1WLwqmDgCEqJgEQj4MIWeAFBtNlaR20ShykwBAptjMVBYENzSA2N7AkAfb4iXOwPfSdd0CQHkiuDWIrwVRT2rSrYOrsr/6TzZVTxCU+V0bIoqFW0J75vjBi3vP/nhg8012SxAKrLfj9B83qtrC9Y+qfnKIswRPbIdqTMWRfWc8T9kTyjpmjGcmbXo7DsDSEYQIHOMjlYdxTpQ7JIxb24/BpAMgJiiEUgKdvkkHgsij5DKITEKY5NJRI54E8VUM3FJ5WjC25UNR03pQgx7SyKlmtIoUDwClaol6vWqFGo0okmqHXgNPDwBm0cFP6YQxVEoFwkk8Aa1KIg0ANLfQaEsjgIsM5o4YAAEhfioM3O+T1WvKKV89ZFvf1G854Pbqwq3TScURUf3mfY6eKWnMua8WoJ0AbH+1SxAyg6897uyff95lnHRzr5XvJLQb+6TyJFYeqoGc6ra0cVWAxAAjIIsC4DiQJCCjKrrilSDwaEisAUoSBVBkChz4CIIRCcCD0Euk8xjXuSRpmHMRfBgtnEUcbIlaUgVGv2mt6Vx0c0PWry125xYc4tAZErctSrhFMWuYlrLYxIhe1VwYKpTkCqBQFSAnbKbCTDRMzFIQVgr2wEYBckAqgOFXQaFDOoeGi2GUzXnZ0I7Jfv/e0Pu5n8+76k4Fh+TPuPzof/37A0IxdS2CSDFOxfNr8z6+FFONDucKZMW8AK/NKZqRAmaOlU9EwbXlX3QAbIwAIqGAGENJY2q20TaABCwqdjtV3QqAiqm1qgIPwzo6YX0VXJrO7lOuQ5kCnBYsHrdVRZqYroov0Z5jmhI0JzYrengYFTELBhUI46nsDdAScIBUrzdAEA3QJpcLnl7PoiSTxwcU12046+u0qw+USnjPht+zfP3IJToCS/QiIIU73n7ksrDnz+DqNzLEjPw2ciBl3wQnTyjRUGmKxfsqa6rWYAYLAqqB5CgHMLuBakIk0Mwk0OiARQVg0iQi7FX/lpkj+nhhxN2P9yxXW0NHOHjpmun8FBWZgYoEF2qHTt4TKA56uHhKbh4qlMPdukyCAAwgDBwGOOooyABdktXuf3ML1q63753/QvTPZVHF0qussSY3r94fvBVrz0XzmrVA0hV0be9Sra/+7Jcm3uRLXbA4qTMe5629hx2/FyeR33XNq76mwVjtRqyWU0BpBGbVYmqgUkmJiXBPMmP5i4L6XDaM4906RwO4rYKUFZjAoEiJqiz8mUSonHtYa/mANXZR3YxgT1oj+kGiD4llO4X9634562VOZsf4y2OqoTBs+5Mn/T7TcF3FVoUTIA5vvmZ+eXZX/zDEtljS9r5C240xw9ul1GlPQvgTHm2XQBhnW7IZukDkTiQNUOXLGEKuFyBPJDRwno6zQ4NmX4JJnDHCaHCDdkrH0/bJH+EAaSbetR/FZRmm3Z8tPCfVXmbbortO2VJyoi7fh3Tb5wuj4QA4snfkXx0y4KfiL86NmXywqui06at6tAbPQsLhdgsQyWK6gnqbDzhckhzbJaiYfAjCnyZhO5kVoeuu+hS4KAq2QQWDYGPoy0L5y2wV90CerMvxVue1WPH0l9tlFxlvTIue35a0uBZa+sBJG/zX58o27fkjzHpl3yY1m/mXShlArNt/CyuTgOEsVlMo3QgXUEL150URYbOctl38lDg5boB0vXLklLKHf3+73cc2/zKq86081eNuvbDmUwW0SmIu3R/4pE1j6yjmic+KfOmyxJG3rSl67tw5tYYUPU2Yjt1StKUujelBkNJmLoXIjU4sF/tCpaqpVmiCwFD0igOxO0cMIslE87Z1Zx6t778YSi0Ax5lZ+7bOH09K9i1tMfxrW+tkmoLewyY+fzFSQMu2aoD5Piut+cW/fTah84eY5f3u/S16xFC/tPXzdPTcsgeEt58ECAtslk2AnP2My1VuA/FSRuEARLgICoTwQkJ1QcHa7YJ+wf7OSigs4F2Gw6bfT/7Vz6ysGjX+wt6jLjhqYxL//oYYqQl5+vfv1N1bM28tPN/d338kOuWnrS3ewZXHMZmQZuMhkwWYSrV4u3ayaYcDadNVywsAwxVowzLflC1Gw4OVqjbQNjuFVdyaP3Qgyvv2mCJ7n107CVvT0S09mDM7s9/tweoyg+7/K1BKDKlkStEu1s5CwuEsVlNW9V1KhJQahRPoZCxjqK5dW54p2PIIZaLuZgcXod0yzm7wo2D4dSjm71q9TVRStH292ZvcZUdGJ1x1ZujUOWR7y7N/vqhlbH9L1nWd9rTc1ut4Rx9oJ66tym3k3CA3LFQQ2jhKWGpWptu9kJh3VSuTQDpZq9am079/tENLz925Pu/Ptl/+hMPotxNzz5ZvOvdx1LHP3Rfj5E3/7NNNZzDD4UcFY2dN+Sbte4AoCmTAWAKaGciD6+rgplCoaH1PIyCdNs/2rZwSw6snLF3+a+/Thx42UfowIo7V9YWbb10wEUvTYnqPen7tlXR+ClKqVBWVmYETwSuuHpf9AgZgJb8gBOYqRg37UAZx7xUwduUx2WwGWbsZH61xveyJuphdbAO6A01vv/mYwTueNNQbzNv3nDfLKYRYrFHZ6iAG5KhmAdyUPboAvZKqvhqrurJ729EzDDnSBZWxsiWIAGgMCoaiO2gKrFEXPJfFNPzrPXA8BUfTPvx42sPWCJ6ZqOdH1yRrfmqUjJnvjXIkjjwWEcBcnjN32aUH/pmOG+2UcwDcBwLXAqE4bLIN0QRz5s0xPGEReXqgVV6cBP7y0JfKUJE5QVeZNGvxn29DqxHzgHWqH3Azf+1RI3Ma66Pcun3I/wlX04DbDPKsCsULMXCADmVEMIB0jiMeRaTZzyDOSBAEFfrq7G+9vi/0Zug6Px9kIoYLNcZC47w+Qixigzg7FrYOdWuVL72Yc1fOBKAY2GWLErSCJ2SywcB8fN10b7BSEKZmOLn/FqIGrSuo2vpdJejlFq3vHlBliZ7beinRb9w86LdO+KXX6YjhBq5+7a1s9lfP/Nw2eGvruBFOyAOGfHpLNQVIeA4jgJCAlClnx58yn7X7xv/IfadR8DxtjzMIxdGCLHoVD2uHXGgP48Uas+88zfWyOE/NdcntWL9/3kLVzwE2KaDTL9YoJ/u549ZEF5vShQHwmIuBew2UBSIMWeo9eES+1svXo9ePO4Lk0nOSrVoqP+d9L1SqnddSOWynhrSMCJ+jiKbAoQg4tr1qKZU9AYaiJ0PxrZrPrCm/ep6PnL0h21dO2fic1vfu3q7pyRrONry+mhqiUrLGTZv2SCEUIetwL6q4jTFV9gTgAfdcYgxO4FLUQGoVGsr+OkfH1DFE4N5vjFAEKXxQ26+U3D2OcjzYYUDVamqCnxU4p6mfPaD7VB3SYLKlffnIeDRqt/gQWUdQIoonfjsDap6IsSEy34rOpJyG74YhQpeQUjeeaayUWfSQvIef/dj1XVwHiChfuIHzQeWnnN+JUaNf+dM6m97+7Jr6U1rKvN+nKYDxBbb/8CQ2R8Mbom/b60BStfyubnhsKhfIo03oaztb/6ouE8MwYIYoBxBCgLA8byUPPrRzGJveWHDtliUK1vNaWlTpJZlkKUc5DLHPlai7mJlk637fydXbHqIi5l8qwWZVoMnrfFmkJZGEEJya2Ptvg/gzX9nieo5NLdJgCReebcYN+mNs3medi29ZVVl3uaL0ZbXRlFbXP/9Q2Z/OKQzAGnLZGQtv2WX7C4axgmmRgDBHC/1vuD5AciS3KyM0ZY2mnrGV7T8BrVm25+wKfV9a++LX0Xo52nr6ej8NVXOW/DOUtWdNQcQo/ZhqXoYBUm8cr4YO+n1rmzvVNcVAsjm10ZRe2z//UPmnBKA7FY8xUPrA4TJIyxdEC8lTfhrptmZeqQrJ0MpXTXRX73jaYS4YlvcZX9EziFdWn9X9vVsqst7/N2lqvvQnBAFCcggVAfIrHMIIK8yCtJv/9A5H518CrLi1t1qACCGkB4U1vV8WlLi+K4FiL9kTR/iObBQ85/ItPec/WsUMfJn5YR5MgHnPf7+UtV90ABIkIKw/FU6QK44dwCy6dWR1B7X/5QA5PDnt+5WPCeGcrwImDPSRgW1WIyCJI57ussoSGnpWrtNKn1Ird5yny1lzp288/z/nswF83OrWweI59Ac0FUy4SyWFyxJV8wXo88RFuvUAuS23aq3KYAwUwQvJZzXdQCRKzbd7Cv8aLEp8coHzPHT//FzW8Ane7zeoveXqq6ADBKevpRRkKTLzyGAvDKS2uNPFQU5NQBRqrfO8BZ8tEhwDvncEpm2EEVMZcntuq8unIGfEUBGMDXv/mHzPj7pMsjhlbfv1jwnhuIAi2WwV4alHHO8HH/e04PNkSnZnXmP1L13qLtg6T8w5/DykUP+YEq4ZE9n6usu2/QMMIBorsO6FqsuGXVQBpl57lCQjf8cTu1xA04JQHJW3m7IIEE1b8DibmixODX+vL8ONkf0yOroovRW7O5JK776EyH+cYIj8zFzj9mfd7Su7nItz4C36IOlmq7mFQJZTA05hGpesCSecwDpt3/YvKUnnYLkfHnHblUHiGhQDuZrFXA1wZjTGEBMEcmHOrI4KT3k8OVvvll1H/ojHznyWWvPa17oSD3dZdo2AwZAGAVhlvTAUQfhWqzo888NO8iGl4fpWqzh15x8gBxhAPGWDA1a0nVwMJAwFziO1+LGPtMhgFBKsVq9abL3+PIvROfADywpt97Rttfc/VRHZ8Bb9NFSzZM1x/ApClAPxmwxIT2BCendAGn33J4sgPhrCwfI+S+t5CwpxyiKvcHe57pzOrl2uyf+JBT4+QDkH0N1GWT4taeAgqwyKAjHmwJ2EMPrN2BJ1+LGPN1uCkI9ZUnuvNdexxzXC5tT77X0unHjSVgP3VU2mIEQQMKcFfWDbpgMci5RkPUBgIw4BQA5ygDiK21Ci6UL6Vrs6PYBhNKKCG/2ot8TzT+PN8U+a+49fzFCqEOpPrsR0L4ZMAASkEGCx7axk510FosJ6ecIi7X+pSE6BRlx3bKTLqQf/eouw1AY0mLVefMyGSR21FNtpiCUUtGX+8bVqvf4M9gUvcLW9zcP/xzTFbVvWXfd0zpAvNmGs2LopLgAQOIvO3cA8v2Lg6ktfsD+Udf99+QDZPVdu7UmhXSDgsS0ESDswBm5ZHimWrXzU4qEfNuA+VcjFF3Tda+/u6bWZsBb/HEdBQk/kFPzgbkbIK1NX9P3j62+e7fqY64mJkN7FfLFMrRYMSP/0iYKQmmV0531yveIyBG2QfdNQCiuuGM96i7V0RkwABKgIA2cFc3xl55DFOSFAAW5/uRTkK4ACKXU7Ml5fhGVyycLziE3m5KvW9eZSMiOLpCfe7mfDUDWvZBJ7YzFuv6Tk85iGQAp0Q2FxpnqQVcTg8WKHtEyBWFZID05L/+eyCV38ua4lyx9rl+EULz7575YT8f4vcVLl2rew2F2kMAR0JoPTHHnEAVZ+3wGdSQMPCUAyV09f7fqZ2pesY7FYiAJGAqjRzzZIovlOfavK4m38CnEcVtsPX/5KLL36bZ3nA50sPxLJUsDlvT6QjroAGFC+vhzw5K+9rkMnYKM/uWnJ52C5H4z37CDBGPSQ3KIQUGihjcPEClvcYbsL/kXkSoEU/+7f2k29+2UU+NpWlfnTLMGQMK0WEFV7zkJkLgB+0ffeGoAovmYq4kRkx5yNTG8ebWEYX8ejJrwxaIFL1h8kuk/iuvgxZY+8y8SIzN+OGdW2lk6EB0gTEgPczXR1b0hFutcoiBnOEA8uf9+Sqn68T5LwkV3i0mDlyHUTzpL19U50+1ugJyEV5m75h7dDtKQxQq4uzdJQeTjH9/or9r+OG9OWmZJnPICcowsOwld65IqWfpVkMp6qVCZTrWankRTIzBmCes4t8BFFgJyHAMx6djJNmgyNTgovj6yWtabat44QMQKlMoICRWiKSYf+MhjCMUVdWbQBkBywgyFYUJ6LBPSO0ZBKC21Sy5XMicVJRO5IkGVa5xE01g6WYKAdyNTbAkR4grsCaPzEEKuzoyhpbKhrCZMBrHFDdg/5lSwWAwgTIsV8MXSkzbocoie1USLH/pEPRbLl798klqz5WngRLO95xU3oIjRHXKFP1mTGKyX0rJkqTZrONEKhyDAfYGLsGFsZknrEAYiaES2AvUjTNViSvkcjo/dy3F9diBb5xZp+LhYXmJZPjIQ/AXDqFY5lCIxEfM2lgSWZbXkKJHNlPhESn1eoGoxwtY9nJiyT7Ak7UEo2dveOWoMkEDqUcZixV7SboDQmu3pvuodwzVvYQbV/CkIW2Iw7yQUizJQjlBNEVWl1qJ6KzFR/aUAQi5vjTtg6zF1lzl+XJfLo6cNICRcBgmPB+G4egDx53/Zl/gPPiHXHLied2auIgmTboyIGH1Ghc7SqmNOBWf1J1SdRIFMBSr3olQuR1g8DMhRyHN2FyGSmWqeJEr8/TGgFAA+HjjbNozNX1PCrxMixx3oLEWhviOpmpybSShMA5AmEyqZgCp5wFmOIGor0TBSKPE5gdSmICqlUYL7Ycy7EOfYgED9SjT13o4sw9qVl9lbsiyMggSoR1AGaQdAKC1z+PKWDKYauZionguIv3IIgFoMlBRg3pkNvPMExaKPql6L4i1L1Hzl/Qmh/akGTmyK3gec4ztNlr5KHnzbPhTdt8u8KU4PQL69dzfRLekN1bwBCjJ6QSYy98iipWvtfu/x38sVG/8E2ASAhRpz/NTbxcTEzxAa3aUnyLZ35wxRjaqdTk2UJypSyb1EK59BKangTb3+a7JkvAlC8q6GSfgorehJ3MevVqQjD2NQEynwwIlxnxIu6lnBGrkPoZR2H5pK6TYBPK4YAs4bVLnwLiCuFIqEImzr/7xg6vsJQrZ6HgZG9vuyIf6anXcTOW8eUBqBsLWYM6e8oaCaf9lsUjlCc9vk7KkDxJtjCOlhria6kN5GgFC62aIWVZ6veA/9SfHmTaJEo5izFvNR5z9riT3vf8iSnN/w/firs/v6Clbe7S3bdgdRFAfRCHD2XlusUcOeiooZtBYlj243NWxqDZwWgOR/e6/hahKeWVEPu9UBQuJGLchgAPGd+OZ2qWDJm0iwsvTsAKAC4qzljv7zJyNTrwMdXdRdVY7JGrL7h4Wq/+BvgWoiYF4TbONvFy0D/91aG9RfOF1x/7QCAFmAaoCwpYi3DbkdmVO+bK1sw/uU1g5Ua3e+QuTSaQAEEB9xQLCOmY1MsQdbq0t2bXhJ8R26Xz/Zk8jAiT2/NEddcCtC9hOtlWX36wEkzNXEAMjFbWKx1Mrdl/uKP3yPav5IPXSXqKo59frZpqgxy1vrQ9X+V952F268hSIBiOwFCrwndsjttzpSpy9prWxb7p+BABHUuAlPpvgrcjL9eR8vRqCmIo5FqwUysANROXP8t7YBN12PUM+KtgzyZDxDqStOqvz2KU2ruAIQjgOqYd7S7wk/iX05MjKz1ePrmBCq1R79G5GL5jPwI4QUAC6Xt2c+iMxpK9vaZ+ormKJ69z1DqZxJKXEgxJUhU/I83j5qM0KoVS2fr3JjKqXH11DN3U8nAEBqMXass1in390W2aizAJHcR4ZIeYuXApH66bsgVQGLSTtscZOnophxrZ4y4Cn8ckR11rL1mqrYCTuQQVM1leC85LEP3mBNOK/TCQJPG0A0P8tqErCDNHA14R29nkPEPRrUqgswFhhpCRxPYKTYR5ir4Wy9X7b2nv+X05FkmtLaWF/lmsdAq51LQdNPLERc5A5szbjGbM5os6Co+A5Pop5DXyFGRQCxaghg80bONuReZEpoNQsL9eacr0nHFlJNmkipZgZ2VKKQ8LwQ2XcBQometoCMHd0mudY+r/lyHtQzkxCWAR9X81zEO6aYOY8ihFqsxwDIkYAWi9RleGcUJKZlCsIosDf3jX9q7iO3sZfK+ks1P/ARgz+z9b7jqrb1v8BS+sMrG6TavFGMXSWaCkSRieBIXZYy9cVbEEKdYrVOD0C+u1fXYoWn/Qn5Y3Ec4e09fwR/0ViEEYcZ9WA+KCgIEnawkQaYjziKrb0fsvW++bO2TGRXPcMOVZGqV96hKeWPAFUSALOMghrlTQMeNUVMfKY97VBK7UrVmm2I+AYwHt44woMSxEW/gp3jHmtJfUn9Bf2I79gCqtVcyw490ePBEVdCHRkXi2LvXe3ph6JkX6RUf/8FZUcesWhAtosjvpgzpT5gcs5o8bRjb8knDeJBwtS8rQBEqtw9WC759Guq+ZL09xtI9sBHDv3Elnbb7LaOoXz3y+94izfeSJEIjIoQRWFVVUcPvP6K6AFXbmhrPWeODBIASCirSSizCTs8R1C4iN6vg79kJlBvH8yJjQCC2GQSGbC15zre3Ot2c8rcnM5MQlvLst3W7/5+CvHn/weIP1XP5MFWNeKrrfaLxyFzXLtTFRHXj0s1f8kcdq6QURk725mrwbY+VyNL+ndN9Y2WltrBUvSAJhX+mRXSOSOqAhJjv+Ajz7+8reMJPkepJ8lX+ekBqslOXYxg/2PyCB+z3hw7cjZC/Zq1OTUGCOsNY3X8YIq5qEUZxH/iqwfk8q9fBMTMG4G0pYyC2Pp+Z02/b1pbx1FzZOnjNdlLn6DIDJQBRCOg+l3gSJv+QtLoBx9qaz1nIEACxx8EbSCYAcTkixn3ZKKvcP0tUvHKvyHOIurUl1ERnZdhJ0WxY9QYQ+IH3nne64VS1IP9+l3aKr/dmYnS1yA9YfNVbvqSKKWTENOq6QtaA05MXGd2zpzakfo1z8GF1Ju9AJCo14coo5AqIFPs5yhizFVNhQ4r3sIJ2LvvawrEZvSBAKUK5e2ZdyJL+lvt7QfLBuOv+mQvUaszGDFiMeUshQ+bXzFi4q9E+4hmD8FpBJDQQmcsVssA8eYtWqG6Dl2uU+FgNCLb+Ezxe239HxnW1mM4XPkr76o68O/XQwAhBDRZAt6etCdt+qujOhMGcVpZrPohtwEtFua12Al/7l919PsKk1rI/K6uRLy1aYCwSUW01hx/8W/E+Olvt3dhtPd5v/uHBzT3jqcpiBaWz8tYnCrwtiELTLZxbDdv90V9ubdq7j2L6gOEDRd5UOSYyUh0bg+vlFJqopVbVlJSO5XqO4WxY1OgEu8YORKZkjqk3fPXrPxGk4qm63JyCCAKIC7qB2v8NZObk/U6ChAWsuA9+sIuzV8yGBAXBhAFMG8rtg16YGBLp4iFz4m7aP0NlXv++R5FJoOCMICoKjvtz5U8/dlBVmtKo8OY2vqiTh9AdHf3cAoStKRzWuzIx3U7iDf3nfGq6/AiqlRlABfYsRFzizcoSXD3xLx9L46/8G5r9PhNbR14e5+jvqJUj+ublaedmf4AACAASURBVEB8gxlrFeoDJWCNmHwJMqd/1d46daokFV2l1e74VK+TUQ+DZwPEVL+W1KeRY9Cj9QDiyrqG+vPfoJRGskepfsAsAYpM2Xx05oi2CucN++qvXbNU8x+bUw8gjIoAdYmR0y4Urf2adAzVAeIL2kEaGApboCAsl4A359mDmlLZR1fhs7Z0YGqAAPvs6Q9mIEvj4/GammNv8ca55btfXgJY1NkrXQ7RNCCqoqVd8PRY0ZmxoyPvhpU5LQAp+O7eQDyIkfanLnEcAswzgCwYjEzJh5hBy5f9yt2q7+hCSrVoYMfmQgOAMKBQAsic8JG9x5WPIEvfRkaljk5OeDm5Zt29su/gi4B4ZrEJyEX64aIeS8wVQxBytssCHeL//SUXaa5tXyH9+ACmyQqAhJ2+KTi2gTNlctAFhFmboerIEqrWXsKEWl1W0KmHCiDGrOQjx83s6Fgl9/p/q95Dv2LKAp2CBFkl4gfBOvQxk3PyU00uzpYAEnfRfNHZtC+WHvR29IVdxF8aoCBBgOiAJ45+Dw5C5p6H2zIeHSB7Xl7CZJlwgGiKD1InPHmBJWn42rbUc8bIIAwgmr9B2p+QL1YdQPQdtqrK6Tnx2qua/8QcwLzAyHEdBQksVF2DAxJvH/RHi3n0myg+s0ujC1laIV/ZmtVEqxjHdim9fd0uQwHz9mPmqDmZCKF2W8D18flOTCPuHWsMLVaQbTNgAoA94MgYgczxuuqYegsngOdwQPZgrRs7NgUFOFva88ia+duOLgS/a+Nbmu/AbUGLeCgRNZGAM/VYbY6+8lKEws9DN1qqR0F0UAUWuu7u3jxA9LL5iz9WavfPQ3pGFF3HC5RpKDlrsT3j9lEIpbUpx4C3cP015fte+UgHSEBIp5oGquKDHuMXXmJPHtUh6n76KMi6e3dr3rYBhHVSch0eJOUuWk6JPx2QwM6Gbqz6BQ0QZykzx02+mo+awoxkYYfbd3TZGOUUb875Us3X6wEJetij3n5QQDclbjBHzpzU0Raov2QGqd3+tc5i6XXiMCqiAkQOuRqZEv7HNGjgOvQc+At/wzaJOurBFqUMOCJjPjL37nD0ngGQgwGA1OXY1Q132HrcEn/jwKZsIp0BiFS54Vp/8fJ/IITNiLBBaUCIJPERGf+zpt11e1vfofv4hl9W7n/5XUBmHSA0wGapsgd6nv/E5bbEUV909P2cHhYrCJCwmPTwzIqxIx/XWazwQamVOy/z5C9eDljg2JnpjWwjuryqABZjD9r63jUZIUeXucPLVev+Ifv23af7gzUACG/p85HJccF1HX0B1F98Gand9UUdQBhMAoe7UxXAlv4osqU9TSm1QOWWg0CkXrpbSIh6sB1bARwxYiYyJ7XZAt+wv1LthndU/6Ebg4FPNCQT6PuMaou6KiNIycLLdgYg+uZX+eN1qvfwdKyp8RQ0lTMl/iQmjH+lrQI6q8OVt/K+qkP/+UdTAEka8/urIlInddhWdtYApKLihwiz+9AjatW2PyCOGZ4bGA8DhiaEqB/b0j62ptx+Z1dY2fXEdGUf/UTUmmG6DFQPICqIlqFPC47z6gnS7QELVUsvIZXbv2wWIJbUl5Gj//1UrhgHNbu/B13d1QAgoFEcMXYoYl6tHbyk2rXvqf6cG5oECFWpzXnZRGRN29yw+s4ChFbtdPq8Ox1I4XkJJIg0p3og4cLytlIP1p+qfa+95Spcd1uQxQpREMkN8aMf+L+otBmfdnBaTpOQ3gEKou82RasHKq5dL2r+oosR02qFW9dDbA8BhMUTgr3fk+Ye173W0YkJlqP+0nRv1f92UgC73l5DFsvS/w2T2DPA2jBBu30XUWuvBE/+EzofrjNXQfbN4MnBlLAURQ6ZR73H7gf3kZeM45bZxZisoAaLatgy8GrgETsKvh2XAuwweKRqvCrtf5Wq5eOCxso6CsKMhhKYnZfO4m39VnQ1QNrR2SYfpTUF0SX7X1iveEoyKeXqs1iSGxJG3jfH2eeiDp9LedZQkODsePKXzdJcu54DzZcOzMoeMB7Wd0VhDm/R202OAQ8I8Zd3Kom16j98mb9i9Rd1auYwGQRRQLxzO8bmjQZag4vXYJH0/5OgQM++s1+Me7q8gTFFFIZRxTMqKNc0Aog5/kvkyJxHa/e+A/6SGxsDRAcKAcH+MVAoo0ADthG9cf0f3bKtd8iAVOi78Y0hgCdq7eVApKgQ+Bg4A8Y7ZoQ0R0//JW/q18i7tjEFCQjqzJIed9GdonPcm50FQXPlmYHTdeC1G12lP75GNbAQwmYiTAbRAfLAPGeflt1lWurfWQcQNhjP0bee0jxZ9wLCDt3I1IjdYlZ2BbAtfQm2DnzIGjuhw4YipXbbQ5Jr83PAscjAMEt+QM+EsPkwYJzFYiKR7pIfkB/CqAETu+u8kQ0KERTImQ0AU06q+14HKN350OT8AWyDnoOa7ZtAqR1XV78hSOtLHlEVYW6zRkitgcKAmjYgp7B6DGBo+h3jfvA7k2F0E4wKFKlBgOjUS8cS881SqBA18RlR7Lut4WJqkcWKvfhWMWrc4pMBEObVULv/3xmyt+oVyVU8DACbgirecBYrcdT910X2vvCjjvbhzABIKHlcILt7wA7S/M5Randn/+cLKp34BSCTcfJOUPUaMiKydSIBHz3pEUt+7AtodMcCrOSq716RvfvvaZKCgAZCxLh7BFPmf8KQ0ZF3EVyxTZVl6mMTVGzOAiqnhFigkA2EAQTX4JgxYwAi2EYQRGh7+9FSH2hzXrGnEiC6Jg+28yV71orWuOEjlBMb/ibXHBmKhJgyxV/VW/eOaUBBfqYAoUgpXztSLl+3gmi+ZN2XpymAsK0RoQpz8pU3CPZhHdKFS5Urlyi+o3ObA4gpctqVnJjWiDdv7+ps7nnmj0QpjYeKDVlANWeTAMFCIY4eNAAgoVOu3S31uTm/qFMLkNoYuWLvRH/Jliulqn0XIsHpM0WPfEZRfOM8Bd/dxvQX5wZA2mEobJ6KFFh8x7+5Vas99HcK2Ax6OEGdhTvEDgGhSIjYYEu66g5k7tNub1tf2YqVmpJ/qaE8ashiaWCKmD6Tb0eAU0eAQ6mvF5T/dBCAWAwGKchCsU8aC0XOw9FTB7QlQKoj7bdU5mQDhFJqUyt/GipVbpusuLInANWSQEjwAua3qVLNWkvKhduUqoLHXLmf3xNU8zJWi/lkqX43JI4+G1msLgCIvlC8WT08hZ//jUil14MedRiUR4Kevwb3jxDIYE583RZxyZ9RZEqr0X7hC8JXvvwrTSq4iFnQQwAJKgaAAeTiy/gOhMm2Z6FSWt0XyncdAKBifXAwoGgAnOUojpo0qCvU2u3pF3u2RV+s2EtvEaPGthp+3LBN5lIjVx3updUe7q95jgwFQgYBFvtTQnyq4t6DucitfMKUn2wJkw4yT92aw++8XHPsi3t1gASMhAZAXJA4+oGzUAZpyhdLz7AYkEFGGb5YbXlZSu2O8f7CFW9R6ss04jOC7idhuz3z1eKESsGWfq+YMGeZEd7atstbvuJzIuXNNAASUPOecoAwCvLjQQqaTkEMGmJQEUZBEG/Lwc5fZLRnXG0bfetPNQ8QP1iSZt3I24e/13otAPTYMbPfmZtIa/YnIUVJ1wANo5p3NKiu3kRxlwIyHeUjMtdYk8avQZaB9fzearL+/XzNsS9/wwy5db5YBFTJDUlnI0DydQrSlDevkXo0th0AYZPvL/n6Trl8zd8B8Y6gMa+xxkkFTnBmcfZhV5pip7YJfKxuqXrVh4on+9qgFT3Ixhn+WKeKgrgTaMXWLKBqJAunCmmpUAAgHAPIpNMHEJbVJOiyHhb4ZEmafQNvz/ygJYAwp8Xa48sieYr78Dy9UPMcv0iVikcQ2WUGoJW8rc86c/SED7nYAWsRimsyQVz1oX//tTb3y4e7AdLCTPuKlryhVP10J3AshqhBcFVwx6cKcJZe31p6/OqytvLrcs3al2T33vubBUjk9Kt4U+8OuzK0aXel1EIrN2QBkVJCqiY9jjAog5iPcVFTGYt10oPGGvbXiAfpBEB8lam+ym9+q9XsvY6qtTFAWTyKAtgUd8icPPNBIbJ1R8PqQ4v/Upu76tFzCyBh54PUnTJl5MVqLwVhL81XtLIX8WV9qPlLJug2i6BArYMjzOsXwMXbB75sSZz9WFsWp1yz9QHZvflFpgeoA17A5Z4qYHJOuZY3Dfi4LXV19Bmm3qTVP/xA1ZqxQS9iPXREZ7OYkG4q5KKmMSG9TYkaOtqPpso1HTDFQm4laI2C+Cu3XKKWfv0HSnwDKaVRQCjPrPbYkrgdWxN/bTH124/iJraaVrQm+4OFNUc+WxAKmAo6K57VLFaTSRsMgMS1k8XSlwqlyFv8/izqOvZPQqSeQdWvboALNyQykxlnPiJGDHvYFDPjk9YWi+o5cJm/es0XugwSHgcSiCY0RZx/B28Z3O4w19babXhfq9nxIcgnrqW6IiKoxTJc3hHHV3DOCxlATnkapOaTNjAZZE6zLJavbONUtXLtQtC8YyjRLEYINbPpcOW8Y8B8S+qvVrSVIlZlvf9E7bHPHm/krOh3Q9KoB66L7HuWGQpZ4jgmg4RnNWEhrEEhPW70wjYL6eELidIiqy93+Z80qeg+CshqqH6Za0gDQyKLOTDHf8Nbh/7WFD2uxfQ6LHuIp3LlTgBqC1GigN8XAgVEx3kPC9YRz7Z3wbf3ec196GHqy/mr4WoSZK9YLWxRIR9vnZCBLFHt9MVqby8aP9982h8/WBJnX8s7Bjeiri5XTjwqW/Uy8RbMC7kIsTFpEmBrr49tfe+/rT3UsPrQf/5Sc+yLRxmVD6p49cQNkguSRj14dgIkPLNi3RFshpAeN2Rhk+eDtOV1Uu/xnu5ClqWvdhIgnp3tFghwCqMk+m6lgOAY+KIpfvBTCA1qdudlbub+0g+2ampNZkMFgB6Pbhnygjni/E5lzmjTuHwnLtA8O76ioIce1gnqerAYJbxj/BgkxnU4tLQtfWiaxQpLPRqeWZEwgMyZyzsylzWWW767Wq1YvZS9bV2ZEoiBR0SVLT2vuoqPntiu7JKVBxf/3ZX75W9DAVMBa7oiuSH5bARIblhu3rp8WKHs7mrckAWDUUSPdhv1gi+C+rImuvI+WAFAo9iiDgU41aMk+u5LxJhpN4gRY5c2lT0kWJ9cufpN2Zd1e0M2i/H/grnvx6bI6dd2dIE1Vy7oVhHMQUxpdZRWufUAJVJiwA2szmBIVRAc4zsVD9JCP/SEbs3NT9OZFZnPh9QkQHRW+PjiFVrtoZlGoopgBhWVRWcW2QbOH9De8yar97/1ek3+6rsaySB+NySPPgspSEsAQRwvx4/6UyYyp3Q41xU9ttbss9beo1b+oDsZNg8QoJgTjwkxc68W7T13N7dI/J69M9XqtZ83BRCOj/vBEnP1+K4HiCsO1KqRwKesYYuTea4S99Z3ib/o+iCbFXI8pDJwtsEPctaBL3V1P2RP/hjByqkI9djZJAU5sfRTzXfkqvonTLUEkJpoT/YrB4jqTggqHIwUQxLwlt5rLX3vndbWdD/B/pTtem6Jt/jHuSxxXDAvFnM5YZb0sxMga+7ZTYIx6aGkcYwb0g2FUvxoBpDUI5152SznrOba93fizZ+LeOaJW9+AGNRIIQAFiVErrD2unY+Qo7SpNqkrO87n2byeaK6B9f2+CAtHLbLEXpqBUHSXpdxnfdA8e+8mctE1vPPCGUELOfVkX674DnzKMtXVsVmMQ1EAmVIWixHn3dqZOWtYloHSe+K9z3lL6iaTc8rTzQDkf5r3yJV6l4wsKAbLpDEhfW4jFotKeZnuo2+y+BrBcCqui2EXnCM/tKTedH17x1Dy04Lv/RWHJlHEUo8abiaGDHI2A6TB+SCGkK7LIF0CEDbJvuIvpmreAy8R1T0UNenQaKRGAMTV8pbkZ8wJ177U1BkdjC2Qq1cvVLzZjxupRoM+X7oKQLJGXTIeiU3vsO192bp0QSmn1axbTRER+YgLpgbZG5YwW6n6cTlorvF1WU1YCZb1PmK74Jxxfls1P23pl99/pJ9WsWqX4BjxgBhxfpOaOu+JpZ9p3pxZBkDCsqE0AxDFdXiSr2Dx9/VyYekCug+EqPFvW1Kuua0tfQux05SaT2x8YJ/sKe3LnH2NpA0UKNF0gCSM/s110WebFks/gk0/YUoMOyM9pMWS4sewvFidoyDBCfQeX/KQ6s36AwISo7/EhhqtQNogzNuOcabUO03xVzCWppHrtyTlZWhVX68gRO4bqieQuIG3jZ5vdozucMKEhgvC78/vi907d3DWPn/irENeDr+veXbfrfmOPqtHOOqX4XaCEa4WoieMRiihU5Q3vC1/zbp7Nc++hdboK8Y29z4aASRIRXSAzJ7LO4bWE9IV9+EZvvzFXxvHWQSoDQMW8QEfNeY9a8oNN7YHIL6qg2mVO57do6qSQ+fUgnmxgs6KY85GgHwzf7fmK9XPKNQpR/0TpqSELgQIy0ToLXjzQ+o7fjlFJkGPHQnZRgLBSbrlXQHe3ON7s2PAPGQf1+S560rN2gWSe98jgARznfFRA96UstzkvHR2Z1Jchu2ISPXsfpD6jy4UYqYOQSgqL3zBUEojlMrVqyh1j6M6zQ3u2ipga+atom1IlwQoUbrN6is7/D1CQpklbs6lzS1aAyDZsxqdcsu0WE0BxJVzga9g0bd1AAmwWEQCwdF/laX3/GbbaqoPnuNrZ1bse305RQJmlKORu/voB6+NTr+ow4bc0xIwxc5JP1UAMViWsiT3sXc2gurqYwjaQZVvGEB0SiIDbx/6tiVuZpNknlJq9pUv+4ooJZP1hMs6sPQ/PmvkrGFNZf1oz24YYK8ccuVnuZzY41PeMfb2pspTuWyUXLt+LQXiCAm6LHmcELPJHDljYnvbbOp51Xvw/3wVK/9rjp07WbCkru8qgFBP7ghX3r921BPQdTWvAkiMO2Tv/8jgljSKDftRsfeVtzxFm66hgO3nFEDU0CGeLCcvE9BDal7JOfbxTHMXsViBRcfLpatmKq7t71JKHfVZrTD7CHtRGCoEx9hHzdGT/9XwZegqypp1o0A6vphqNUNCYAONYHPav6zOi+d3dnHKtRueJ0rZjSb7gCnIlLm/SYDQAoviPvFLIuW+BLqHL8uTxZJVgJu3jp7Hm/u2y47QeJyeZE/x+2t4MfaQKeb8X7aUztRbsmSZ5s6ZXV8GCWixmqAglBbFurIWHQLijamzgRgpRylw7oj0+wcjS3I9qtncnPqr8/pU7XtxuSky/d3awk3PBiMK68WDjP3NDdF9L2rRYbKld3Z6KMjq+Ubq0UBerKAvVlCLFTX26QyzOeFoZxdbPdakpiBaqv3mYcWf/9ugAbGpACs9jSlv22WKPP9eIWJ4o4QPjGWTK1dcoWo1f6Cqa4SRDpVFpIv5nDh4vjlydIdzU6m+n65XfQVPclz0Yj5iyrMtxXd4PPnJvHr4LtCq7iFEiTaSyamAOccqk3j+Hcgac7wj80ddrjiv97MnEVUuQKbed1iipq5rqR79lFv34Tl6qEFDLVYyMxTWl0HYeYrevK2fau6cMDuIYQuhRCJi3LS7LMmzWnXdYWlpy7YteAlha4wQM+KPriOfrFN8VamU4pAcwuJBEkbePT924FUdlg9PI0DCT5hCdRSE56TEMX8Z0NZdpD2LgLr2Z3jLv3meaK4ZuhW3XuxImBoYVODMye8j06jHLFEZjXYzZl33Vi6/HqnVvyLUPxYosLSIgPmYL3hLrz+LtrFb29UvWhmpuXZMJSDdSxSp2hQ9/iGEWk/cTGlJH6n6p3sola6iROqt94Flheei/irymYuRLbVdZ6BT/4F+3upt8wCRazCyfGiOm/v31mJMvCeWLNE82XNDFCR4kKcupF87i4/IbBSO7CtafoNSteVVoFpEHatFgWoyc1RcIyRMv9PsHNPsBskSNpRvf3UWUfwPWWJGLYjof80XJ358cpHnxLZbwzO8MzuIM/2y53qMvf93TXADLBu63JrNpcsBUrp/ZWJ10ZYoUXTU75MIIIIIqr+GU725KzSlujc7HMcQ0IPnpOuqXtWWOHUmwkoeQioC5EN6ScFCZWBjksBuHpuPEoe16LnqKVufxCu5ThmYzMEu469JjJ4hVa1/DvRMbeG2kfDPTPBFCmfp+6hGYRXwoiayvlCesjxS9phhuew0WtW9+Tq/68BdgMgAAGDnFCLenLpIpeo/FTQ0Pyqqd3VzQNEt5ZU5Dkk4kYiQNAIR7TZNqzKZ7eMfRGKvekcetAQ2SksSZNeeB4hSMQuApBKq2gDzZRyX8Geg5lWmyNSilk7O1U/IdTujFN/WHlSIuELzF10DvH2tJfbCR8KzG1LvTynu6lK7Po1sEtiMigCKv/QV6i+8oL7alqXekcAcM+lOSfFsCBXQ3wKhYtL0ak/2228SuXoqpZrdyOjCsqcwSqIqfGTGQlUTl0X0mVgCkO4DWEYA5iAoz7LUVG2Po55jQxTF+yAAtz9uzJ/vYzJLdd5X0yv2LP6QaGocm1pKKKiyHyxR6esjEideFzNsDlO8UCg7YCnIXplQe3xveub0x7egmH4tnoPY5QA5sffTW1wndl0gmKxUz/HEBbLNMhkDIdBUt81Xsv0SljoX83xIg4X1DZ35TREQowZuM9l7HKVEFhDIPEuogzizijmLBkRGlrgLn0GOXk3y58HFJFdsvE3z500BZKJGng8j3Q5GoCqufdewnLOGo1zjOHYmdet6XsQjwZL+JcWCBxHJRLFZYbyA2T72SWSO0zOPU6lokN+747eav/AqBMRCMRU4Pul7wZz2gru8cHNkKpUA7ASYG2EaQHGxG/O8D8eZ+5hBcAyT1LJriJI3i8PO7WLk2PsRMupt76V6s66R5X0PgOoaTIGIABzmTH3eMJliFpfXbMmOjR2k5uZKNC2tJwU4jlg/kngfBkevOIUIF6je7Js16cRg0T7kNTFy6sKG7J1S++PvVKlsqAGEgEGQEKx4si7H1Bcw+tWpbdkxbpy51w5kij2qGwODmZKJQs2x055km7cn/4NFxFc0mvGnFBBnHPfCMg9pmugcuhhZ0j8B2XNUo6UeTOJMYIpI0zy5l3mLNs7Djt7rY4bffHsw8z2br+If//Kf2oJN1yBsFgkBpMsiqqI5B876I5bFpYTzaqI5ro+nKmd26aFvZmVc9vzl9p4jm/WgYHV2OUBKDy6/qaZg62RBMBPdS5BRCMajMyMgcxzkOEoJO5gFCGanuzJNZQBEurCOAHhexAQz8OjLhDEOwAkcy4jG+EtsjZ/+fOsA2XyL5js2EZBAALPoiUDGQv1ADY1J5sYrCyXJCaYyDfuR4ziEMNvsdXUVI8cIUUG0jnkqBBBKBfeJZU7BmjwGiOtqopRfQIgnEYDUYD4yF/HRe7DgLMBU8AIQUSOeWEK8PYF40igoMRwflY+5+GUyLfnMZkuo6Oj574ztkGqyE7AoXqbJpbOoVj2WED/BgF0UR+7luJj9zNeJo8ivUslBNW8S0ar6ULW2LwIwc0LiVo63v8tR7gfk/EVVQ4D6a7f8hsplg4FidpxVwMJKKduwjKAUZssIljKS1REkcOyQhvoAkak5ftpzIBYdkQpyUinQW1T34blEqUhnxj2jYkop0VwIqIedtE0o8mqqIlKimLGYUGiKynzfU5K9JGHCwrJwFqli9/s9ZU/eE7Wlu2drvtoIlkiOUkKpRipBsFcQVQHF57KLjqQKZ6+Ji1L7XbMYxce3eBJAlwOkIuezlMrD22N5i1nfhAWdswk4oAb+8MihCgJnGONC2ToNFoiRbZX4ed3vJHRbCD2HVA1ZnRNzUNzAFgNpaOXGVK/vSAwLxmN1hiiLDCDwVhUEvqU8UDozoCgaBixzohCWUlSVsEDPO9xwYik9EilX5adQWtUTADIA8X0BcYl6Ll3MFg9Lw8ipFAkywrgWASoh1H2UB5wtc72P2mwD2yUvNEVhmGsIwOEkpTa3JwU5nRDSDwD3QSBEIKCYgqqyw5cQJxBMsUwRqkVEKdLU6mxejM4RSL8jKLpvky4z/upNfTW5PIICT0U2HQpjnGQd84LANcikb/Bgik/lRVE11nyALZNBAVuPGTnMIZF5DEhlq3urNfv6AjaNQJQMJKqUAuy8dFB4ppqjSPQDNpURjSukmjdLVVx7bImXZFt7Tm2khGDjL9n+Sqbkzh0OlB9JZO8A2e9N1DTZxHFmD+LtBZQoe3y1pT/EDb3uQPLgWYWt5QDucoC0lzU4155nLx2gPF7xFicTtSoBgc9JiNcMhGLAJoUikw/zjiokxBaLYsxxhKKalVM6MzfsHHaQ/AmqXJpEtJo4Al4HEIVj+RI5JCoYiR7MOyt4IbEQTClsofg7015nyrI581cd6EF9eSlUK4tF/lobQSoHGqGYs8oUm91g6VFqjUgvQPY+TRpxw9uvqMiOQDU5ad6yfSlyTUmUSjSBQ1gW7L3KIuJHHYlMn3CsNWAE66sHEGtsvwNjb/qMGWpa2V07Mx3dZbtn4OyZgTqAPJ9BbDF9ssfc9FAmQlMDOVrPnoF097R7Bk7GDOxcctM3Vfk/TkffvzhMFixR5ePv/LhfRw+CPBkd7K6zewZO5wxse3/uj64Te8eiLf+amidLtfFjrvsw3Ro7oMPZ0E/nYLrb7p6BrpwB5jXxw6IZWaqvNhrt+Ojab6sLtk8ZOuftCbG9f/FjVzbUXVf3DJyNM+CtyO659b15WWZHYh46uPqPLxRu/+DB/tMX3JI69pZ25VPVNTflWdZyAIh1mxRIS9Nac1E4Gyesu88/rxkoy/5myp5P7vouNv2CT1Hh9vfnHfzmTx8mDZ69KHPm3+9sz1Qw36TKnE19FG+Jo+boV1FmZ7wrdcpfNp0MbZiv8ptUUrsvFouDyy3JM07KmejtGXv3s+fuDBxd/8Lvjmx44dk+MyJ1VwAAAGhJREFUE+9/BPmqjvba+u7sfWZH/PExN/9laHssuuws87yczydQX+l51UdW3+OI73u076WvTzwZ2cZ9FV8/rlXvncXZB64wx1/655MBwnP3lXePrK0zQBcuxLsH5X5Zkbth+pDZ70z5f93Gy7mMjdkfAAAAAElFTkSuQmCC" src="<?php //echo $sel_ImageCode1;?>" style="width:100px;height:auto;" ></div>
	
	<h3 style="text-align:center;">Selected Items</h3>
	
	<div id="bought_things" class="rightsidebar">								
		<div style="display:inline-block;" class="base_price_block ">	
		<p>Custom Wig: <span style="color:red;font-weight:bold;"><?php echo $price_image;?></span></p>
		<p>Length: <span style="color:red;font-weight:bold;"> <?php echo $sel_lengthVal;?></span><span style="color:red;font-weight:bold;"><?php echo "$".$price_length;?></span></p>
		<p>Texture: <span style="color:red;font-weight:bold;">   <?php echo $sel_texture_name;?></span><span style="color:red;font-weight:bold;"><?php echo "$".$price_texture;?></span></p>				
		<p>Color: <span style="color:red;font-weight:bold;"><?php echo $color_name;?></span><span style="color:red;font-weight:bold;"><?php echo "$".$price_color;?></span></p>				
		</div>
	</div>
	
	<?php //} ?>

 
 
	
<h3 style="align:center">Order Summary</h3>

<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
	<thead>
		<tr>
			<th class="td" scope="col" style="text-align:<?php echo $text_align; ?>;"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:<?php echo $text_align; ?>;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:<?php echo $text_align; ?>;"><?php _e( 'Price', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo wc_get_email_order_items( $order, array(
			'show_sku'      => $sent_to_admin,
			'show_image'    => true,
			'image_size'    => array( 32, 32 ),
			'plain_text'    => $plain_text,
			'sent_to_admin' => $sent_to_admin,
		) ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr>
						<th class="td" scope="row" colspan="2" style="text-align:<?php echo $text_align; ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo $total['label']; ?></th>
						<td class="td" style="text-align:<?php echo $text_align; ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo $total['value']; ?></td>
					</tr><?php
				}
			}
		?>
	</tfoot>
</table>



 <?php

 /**
  * @hooked WC_Emails::order_details() Shows the order details table.
  * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
  * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
  * @since 2.5.0
  */
 /**
  * @hooked WC_Emails::order_meta() Shows order meta data.
  */
 /**
  * @hooked WC_Emails::customer_details() Shows customer details
  * @hooked WC_Emails::email_address() Shows email address
  */
 do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );
 do_action( 'woocommerce_email_email_address', $order, $sent_to_admin, $plain_text, $email );
 /**
  * @hooked WC_Emails::email_footer() Output the email footer
  */
 do_action( 'woocommerce_email_footer', $email );
