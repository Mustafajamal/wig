<?php
/**
 * Plugin Name: Post Love
 * Plugin URI: http://danielpataki.com
 * Description: Allows users to love your posts
 * Version: 1.0.0
 * Author: Daniel Pataki
 * Author URI: http://danielpataki.com
 * License: GPL2
 */
//if ( isWooCommerceActive()) {
//}
	add_action('init', 'wig_init');

function wig_init() {
    add_shortcode('show_hair_wig', 'hair_wig22');
   	add_action('wp_enqueue_scripts', 'stern_taxi_fares_script_front_css');	
	add_action('wp_enqueue_scripts', 'wig_fares_script_front_js1111');
	add_action('wp_enqueue_scripts', 'stern_taxi_fares_script_front_js');

}
	require_once( dirname( __FILE__ ) . '/ReduxFramework/sample/wig-config.php' );

function wig_fares_script_front_js1111() {
	
       	wp_register_script('stern_taxi_fare', plugins_url('js/stern_taxi_fare.js', __FILE__ ),array('jquery'));
		wp_localize_script('stern_taxi_fare', 'my_ajax_object',			array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_toll',		array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_picker',		array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_suitcases',	array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_carFare',	array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'ajax_obj_calendar',			array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		//wp_enqueue_script('stern_taxi_fare', plugins_url('js/stern_taxi_fare.js', __FILE__ ) );	
}


function stern_taxi_fares_script_front_js() {
        
		//wp_register_script('stern_bootstrapValidatorMin', plugins_url('js/bootstrapValidator.min.js', __FILE__ ),array('jquery'));
       // wp_enqueue_script('stern_bootstrapValidatorMin');
	
      wp_register_script('ins_steps', plugins_url('js/jquery.steps.min.js', __FILE__ ),array('jquery'));
      wp_enqueue_script('ins_steps');

      wp_register_script('ins_jqueryforms', plugins_url('js/jquery.form.min.js', __FILE__ ),array('jquery'));
      wp_enqueue_script('ins_jqueryforms');

       // wp_register_script('ins_utils', plugins_url('js/utils.js', __FILE__ ),array('jquery'));
       // wp_enqueue_script('ins_utils');

       // wp_register_script('ins_jqueryuicostom', plugins_url('js/jquery-ui-custom.min.js', __FILE__ ),array('jquery'));
       // wp_enqueue_script('ins_jqueryuicostom');
        
	   wp_register_script('ins_jquerystepper', plugins_url('js/jquery.stepper.min.js', __FILE__ ),array('jquery'));
       wp_enqueue_script('ins_jquerystepper');
		
		//wp_register_script('ins_jqueryplaceholder', plugins_url('js/jquery.placeholder.min.js', __FILE__ ),array('jquery'));
        //wp_enqueue_script('ins_jqueryplaceholder');
		
	   wp_register_script('ins_jqueryvalidate', plugins_url('js/jquery.validate.min.js', __FILE__ ),array('jquery'));
       wp_enqueue_script('ins_jqueryvalidate');

		//wp_register_script('ins_formsmodel', plugins_url('js/smartforms-modal.min.js', __FILE__ ),array('jquery'));
       // wp_enqueue_script('ins_formsmodel');

		//wp_register_script('ins_select2', plugins_url('js/select2.full.min.js', __FILE__ ),array('jquery'));
        //wp_enqueue_script('ins_select2');

	   wp_register_script('ins_touch', plugins_url('js/jquery-ui-touch-punch.min.js', __FILE__ ),array('jquery'));
       wp_enqueue_script('ins_touch');
		
		//wp_register_script('ins_maskedinput', plugins_url('js/jquery.maskedinput.js', __FILE__ ),array('jquery'));
        //wp_enqueue_script('ins_maskedinput');

		wp_register_script('ins_smartforms', plugins_url('js/smart-form.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_smartforms');

		//wp_register_script('ins_jqueryautotab', plugins_url('js/jquery.autotab.min.js', __FILE__ ),array('jquery'));
        //wp_enqueue_script('ins_jqueryautotab');

		//wp_register_script('ins_tellinput', plugins_url('js/intlTelInput.min.js', __FILE__ ),array('jquery'));
       // wp_enqueue_script('ins_tellinput');

 		wp_register_script('drpzone_uploader', plugins_url('js/dropzone.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('drpzone_uploader');
		
       // wp_register_script('stern_bootstrapValidatorJS', plugins_url('js/bootstrapValidator.js', __FILE__ ),array('jquery'));
        //wp_enqueue_script('stern_bootstrapValidatorJS');	
		
       	//wp_register_script('stern_taxi_fare', plugins_url('js/stern_taxi_fare.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_taxi_fare', plugins_url('js/stern_taxi_fare.js', __FILE__ ) );
		
		wp_localize_script('stern_taxi_fare', 'my_ajax_object',			array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_toll',		array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_picker',		array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_suitcases',	array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_carFare',	array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'ajax_obj_calendar',			array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		
 
}

function stern_taxi_fares_script_front_css() {
		/* CSS */

		wp_register_style('stern_bootstrapValidatorMinCSS', plugins_url('css/bootstrapValidator.min.css',__FILE__));
		wp_register_style('smartForms', plugins_url('css/smart-forms.css',__FILE__));

        wp_enqueue_style('smartForms');
        wp_enqueue_style('stern_bootstrapValidatorMinCSS');

		wp_register_style( 'ins_steps_css', plugins_url('css/jquery.steps.css', __FILE__ ));
		wp_enqueue_style( 'ins_steps_css');

		wp_register_style( 'ins_stepper', plugins_url('css/jquery.stepper.css', __FILE__ ));
		wp_enqueue_style( 'ins_stepper');

		wp_register_style( 'ins_sfcustom', plugins_url('css/smartforms-custom.css', __FILE__ ));
		wp_enqueue_style( 'ins_sfcustom');

		wp_register_style( 'ins_cssptable1', plugins_url('cssptable/pricing-widget-structure.css', __FILE__ ));
		wp_enqueue_style( 'ins_cssptable1');

		wp_register_style( 'ins_cssptable2', plugins_url('cssptable/pricing-widget-settings.css', __FILE__ ));
		wp_enqueue_style( 'ins_cssptable2');

		wp_register_style( 'ins_cssptable3', plugins_url('cssptable/pricing-widget-theme.css', __FILE__ ));
		wp_enqueue_style( 'ins_cssptable3');

		wp_register_style( 'ins_cssptable4', plugins_url('cssptable/pricing-widget-responsive.css', __FILE__ ));
		wp_enqueue_style( 'ins_cssptable4');
		
		wp_register_style( 'ins_smartaddon', plugins_url('css/smart-addons.css', __FILE__ ));
		wp_enqueue_style( 'ins_smartaddon');

		wp_register_style( 'ins_smartforms_modal', plugins_url('css/smartforms-modal.css', __FILE__ ));
		wp_enqueue_style( 'ins_smartforms_modal');
			
		wp_register_style( 'ins_popupn', plugins_url('css/popup.min.css', __FILE__ ));
		wp_enqueue_style( 'ins_popupn');

        wp_register_style('stern_taxi_fare_datetimepicker', plugins_url('css/bootstrap-datetimepicker.css',__FILE__));
        wp_enqueue_style('stern_taxi_fare_datetimepicker');

        wp_register_style('hair_wig_circle', plugins_url('css/cs-skin-circular.css',__FILE__));
        wp_enqueue_style('hair_wig_circle');

        wp_register_style('hair_wig_skin_boxes', plugins_url('css/cs-skin-boxes.css',__FILE__));
        wp_enqueue_style('hair_wig_skin_boxes');
       
	    wp_register_style('hair_wig_cs_select', plugins_url('css/cs-select.css',__FILE__));
        wp_enqueue_style('hair_wig_cs_select');

        wp_register_style('ins_style', plugins_url('css/ins_style.css',__FILE__));
        wp_enqueue_style('ins_style');
		wp_register_style('tabel_style', plugins_url('css/tabel_style.css',__FILE__));
        wp_enqueue_style('tabel_style');
}
function hair_wig22($atts) {
ob_start();
?>
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet"> 
<link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css" rel="stylesheet">

<?php 
	global $redux_demo; 	
?>


<div class="smart-wrap">
<div id="main_container" class="smart-forms smart-container wrap">
<div class="form-body stp-six steps-theme-primary steps-theme-blue11">

<form id="smart-form" method="post">
<h2>WIG PHOTO</h2>
<fieldset>

<!-- STEP 1 -->
<?php update_option('custom_wig',"false"); ?>
<div class="frm-row">
<div class="section colm colm12">
<h2>UPLOAD YOUR WIG PHOTO</h2>
</div>
</div>

<div class="frm-row">
<div class="section colm colm12">	
		<div class="dropzone" id="myDropzone">
			<h3 class="dz-drop-title"> Upload Your Wig Photo </h3>
			<p class="dz-clicker"> <span id="select_photo"> Select Photo </span> </p>                                                            
			<div class="fallback">
				<input name="file" type="file" multiple>
				<input type="submit" value="Upload" />
			</div>
		</div>   		
<input type="hidden" id="upload_imgPrice" value="<?php echo $redux_demo['step1_price'];?>">
<input type="hidden" id="total_price_image" value="0.00">
<input type="hidden" id="total_price_length" value="0.00">
<input type="hidden" id="total_price_texture" value="0.00">
<input type="hidden" id="total_price_color" value="0.00">
<input type="hidden" id="total_price_sum" value="0.00">

</div>
</div>
<div id="upload_photoerr" class="frm-row">
<div class="section colm colm3">
</div>
<div id="imageval_main" class="section colm colm6">
	<label id="image_validation_notice" class="field prepend-icon">
		<input style="visibility:hidden;height:0px;" type="text" name="imagevalidation" value="" id="imagevalidation" class="required gui-input" placeholder="image validation" required>		
	</label>
</div>
<div class="section colm colm3">
</div>
</div>


</fieldset>

<h2>Length</h2>
<fieldset>

<!-- STEP 2 -->                                           
<div class="frm-row ">						
<div class="colm colm12">
<h2 class="">WANT MORE INCHES?</h2>
</div>

	<!-- Sample Wig Image -->
	<div class="colm colm6">

	<div class="cd-product-builder">
	  <div class="cd-builder-steps">
			<ul>       	        	
				<li data-selection="length-page" class="builder-step length-slide active" style="overflow-x: hidden;">
					<section class="cd-step-content">
					
						<div class="product-box">
							<div class="product-img-box wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
								   
								<div class="background-product">
								<ul id="dummy_girl" class="loaded">
								<?php 
									$redux_length = $redux_demo['step2_length'];
									$redux_length_price = $redux_demo['step2_length_price'];
									$length_girl = array_combine($redux_length, $redux_length_price);
									$n=0;
									foreach($length_girl as $length=>$redux_length_gril){					
									if($n==6){	
										//echo '<li id="'.$length.'" value="'.$redux_length_price2.'" class="length-active" style="display: inline;">'.$length.'"</li>';
										echo '<li id="l-'.$length.'" class="line_selected" data-length-label="'.$length.'" data-content="Length: '.$length.'&quot; - $'.$redux_length_gril.'" data-length-price="'.$redux_length_gril.'">
										<em>&nbsp;'.$length.'</em><span></span><h9>&nbsp;'.$length.'</h9>
										</li>';
									}else{
										echo '<li id="l-'.$length.'" class="" data-length-label="'.$length.'" data-content="Length: '.$length.'&quot; - $'.$redux_length_gril.'" data-length-price="'.$redux_length_gril.'">
										<em>&nbsp;'.$length.'</em><span></span><h9>&nbsp;'.$length.'</h9>
										</li>';
									}
									$n++;
									}
								?>
								
<!--								
								<li id="l-8" class="" data-length-label="8" data-content="Length: 8&quot; - $70.00" data-length-price="70.00" data-variant-id="410026475540">
									<em>&nbsp;    8"</em><span></span><h9>&nbsp;8"</h9>
								</li>                                    

								<li id="l-10" class="" data-length-label="10" data-content="Length: 10&quot; - $75.00" data-length-price="75.00" data-variant-id="410026508308">
									<em>10"</em>
									<span></span>
									<h9>10"</h9>
								</li>


								<li id="l-12" class="" data-length-label="12" data-content="Length: 12&quot; - $80.00" data-length-price="80.00" data-variant-id="410026541076">
								<em>12"</em>
								<span></span>
								<h9>12"</h9>
								</li>


								<li id="l-14" class="" data-length-label="14" data-content="Length: 14&quot; - $85.00" data-length-price="85.00" data-variant-id="410026573844">
								<em>14"</em>
								<span></span>
								<h9>14"</h9>
								</li>


								<li id="l-16" class="" data-length-label="16" data-content="Length: 16&quot; - $90.00" data-length-price="90.00" data-variant-id="410026606612">
								<em>16"</em>
								<span></span>
								<h9>16"</h9>
								</li>


								<li id="l-18" class="" data-length-label="18" data-content="Length: 18&quot; - $95.00" data-length-price="95.00" data-variant-id="410026639380">
								<em>18"</em>
								<span></span>
								<h9>18"</h9>
								</li>


								<li id="l-20" class="" data-length-label="20" data-content="Length: 20&quot; - $100.00" data-length-price="100.00" data-variant-id="410026672148">
								<em>20"</em>
								<span></span>
								<h9>20"</h9>
								</li>


								<li id="l-22" class="line_selected" data-length-label="22" data-content="Length: 22&quot; - $105.00" data-length-price="105.00" data-variant-id="410026704916">
								<em>22"</em>
								<span></span>
								<h9>22"</h9>
								</li>


								<li id="l-24" class="" data-length-label="24" data-content="Length: 24&quot; - $110.00" data-length-price="110.00" data-variant-id="410026737684">
								<em>24"</em>
								<span></span>
								<h9>24"</h9>
								</li>


								<li id="l-26" class="" data-length-label="26" data-content="Length: 26&quot; - $115.00" data-length-price="115.00" data-variant-id="410026770452">
								<em>26"</em>
								<span></span>
								<h9>26"</h9>
								</li>


								<li id="l-28" class="" data-length-label="28" data-content="Length: 28&quot; - $120.00" data-length-price="120.00" data-variant-id="410026803220">
								<em>28"</em>
								<span></span>
								<h9>28"</h9>
								</li>


								<li id="l-30" class="" data-length-label="30" data-content="Length: 30&quot; - $125.00" data-length-price="125.00" data-variant-id="410026835988">
								<em>30"</em>
								<span></span>
								<h9>30"</h9>
								</li>
-->										
								</ul>
								</div>
								<p></p>
								
						  </div>
					  </div>
					</section>
				</li>
				   
			</ul>
		</div>
	</div>
	</div>
	<!--End Sample Wig -->

	<div class="colm colm6">
	
	<!-- Range Slider -->	
	<?php //print_r($redux_demo['step2_length']);?>
	<div class="product-shop wow zoomIn animated" style="visibility: visible; animation-name: zoomIn;">                      
		<ul id="lenth_values" class="rule-length">
			<?php 
				$redux_length = $redux_demo['step2_length'];
				$redux_length_price = $redux_demo['step2_length_price'];
				$length_comb = array_combine($redux_length, $redux_length_price);
				
				$n=0;
				foreach($length_comb as $length=>$redux_length_price2){					
				if($n==6){	
					echo '<li id="'.$length.'" value="'.$redux_length_price2.'" class="length-active" style="display: inline;">'.$length.'"</li>';
				}else{
					echo '<li id="'.$length.'" value="'.$redux_length_price2.'" class="" style="display: inline;">'.$length.'"</li>';
				}
				update_option("last_length",$n);
				$n++;
				}
			?>	
<!-- 		<li id="8" value="8" class="" style="display: inline;">8"</li>
			<li id="10"  value="10" class="" style="display: inline;">10"</li>
			<li id="12" value="12" class="" style="display: inline;">12"</li>
			<li id="14" value="14" class="" style="display: inline;">14"</li>
			<li id="16" value="16" class="" style="display: inline;">16"</li>
			<li id="18" value="18" class="" style="display: inline;">18"</li>
			<li id="20" value="20" class="length-active" style="display: inline;">20"</li>
			<li id="22" value="22" class="" style="display: inline;">22"</li>
			<li id="24" value="24" class="" style="display: inline;">24"</li>
			<li id="26" value="26" class="" style="display: inline;">26"</li>
			<li id="28" value="28" class="" style="display: inline;">28"</li>                            
			<li id="30" value="30" class="" style="display: inline;">30"</li> -->
		</ul>
		<div class="clear"></div>
		<p></p>
		<div class="""slidecontainer ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
		  <?php $last_length = get_option("last_length"); ?>
		<input type="range" min="<?php echo $redux_demo['step2_length'][0]; ?>" max="<?php echo $redux_demo['step2_length'][$last_length]; ?>" step="2" class="slider ui-widget-content" id="myRange">		  
		</div>   
		<p></p>   
		<p class="bx-btm">Length: <span id="length_chosen">22"</span> </p>
		<center>Selector is for illustrative purposes only.<br>
		<font style="text-transform: uppercase; font-size:12px; letter-spacing: 2px;" class="gray_text">(Actual hair length may vary in proportion to height and installation)</font>
		</center>
	</div>	
	</div>	
	
</div>
<div class="frm-row">
<div class="section colm colm3">
</div>
<div id="lengthval_main" class="section colm colm6">
	<label id="color_validation_notice" class="field prepend-icon">
		<input style="visibility:hidden;height:0px;" type="text" name="lengthvalidation" value="" id="lengthvalidation" class="required gui-input" placeholder="Length validation" required>		
	</label>
</div>
<div class="section colm colm3">
</div>
</div>
</fieldset> 

<h2>Texture</h2>
<fieldset>

<!-- STEP 3 -->                                           

<div class="frm-row ">						
<div class="section colm colm12">
<h2 class="">IT’S ALL ABOUT TEXTURE!</h2>
<div class="">
	<p>Select the hair texture you would like for your wig.</p>
	<p>*Prices may vary according to your desired hair texture.</p>
</div>
</div>

<div class="section colm colm12">

<!-- Cs Circular -->
<div class="cd-builder-steps">
		<ul id="texture_ul">       	          
            <li data-selection="texture-page" class="builder-step active" style="overflow-x: hidden;">
                <section class="cd-step-content">
                    
                    <div class="circular-box wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
                         <div class="circularPreview">Select A Texture</div>
						 <div class="cs-selectbox circular-select">
                                        
                        <div id="cs_select_texture" class="cs-select cs-skin-circular" tabindex="0">
						<span class="cs-placeholder"></span>
						<div class="cs-options">
						<ul id="texture_images_ul">
							<li image-url="<?php echo $redux_demo['step3_texture1']['url']; ?>" id="texture1" data-price="<?php echo $redux_demo['step3_texture_price'][0]; ?>" data-content="Body Wave" style="background-image: url(<?php echo $redux_demo['step3_texture1']['url']; ?>);" data-value="1"><span>1</span></li>
							<li image-url="<?php echo $redux_demo['step3_texture2']['url']; ?>" id="texture2" data-price="<?php echo $redux_demo['step3_texture_price'][1]; ?>" data-content="Straight" style="background-image: url(<?php echo $redux_demo['step3_texture2']['url']; ?>);" data-value="2"><span>2</span></li>
							<li image-url="<?php echo $redux_demo['step3_texture3']['url']; ?>" id="texture3" data-price="<?php echo $redux_demo['step3_texture_price'][2]; ?>" data-content="Curly" style="background-image: url(<?php echo $redux_demo['step3_texture3']['url']; ?>);" data-value="3"><span>3</span></li>
							<li image-url="<?php echo $redux_demo['step3_texture4']['url']; ?>" id="texture4" data-price="<?php echo $redux_demo['step3_texture_price'][3]; ?>" data-content="Wavy"style="background-image: url(<?php echo $redux_demo['step3_texture4']['url']; ?>);" data-value="4"><span>4</span></li>
							<li image-url="<?php echo $redux_demo['step3_texture5']['url']; ?>" id="texture5" data-price="<?php echo $redux_demo['step3_texture_price'][4]; ?>" data-content="Exotic Curl" style="background-image: url(<?php echo $redux_demo['step3_texture5']['url']; ?>);" data-value="5"><span>5</span></li>
							<li image-url="<?php echo $redux_demo['step3_texture6']['url']; ?>" id="texture6" data-price="<?php echo $redux_demo['step3_texture_price'][5]; ?>" data-content="Exotic Wave" style="background-image: url(<?php echo $redux_demo['step3_texture6']['url']; ?>);" data-value="6"><span>6</span></li>
							<li image-url="<?php echo $redux_demo['step3_texture7']['url']; ?>" id="texture7" data-price="<?php echo $redux_demo['step3_texture_price'][6]; ?>" data-content="Malaysian Body Wave" style="background-image: url(<?php echo $redux_demo['step3_texture7']['url']; ?>);" data-value="7"><span>7</span></li>
							<li image-url="<?php echo $redux_demo['step3_texture8']['url']; ?>" id="texture8" data-price="<?php echo $redux_demo['step3_texture_price'][7]; ?>" data-content="Malaysian Straight" style="background-image: url(<?php echo $redux_demo['step3_texture8']['url']; ?>);" data-value="8"><span>8</span>
							</li>
						</ul>
						</div>
						
				      </div>
					  </div>
                     
                    </div>
                </section>
            </li>
		</ul>
	</div>
<!-- Cs Circular -->

</div>
</div>

<div class="frm-row">
<div class="section colm colm3">
</div>
<div id="textureval_main" class="section colm colm6">
	<label id="texture_validation_notice" class="field prepend-icon">
		<input style="visibility:hidden;height:0px;" type="text" name="texturevalidation" value="" id="texturevalidation" class="required gui-input" placeholder="Texture validation" required>		
	</label>
</div>
<div class="section colm colm3">
</div>
</div>

</fieldset> 

<h2>Color</h2>
<fieldset>

<!-- STEP 4 -->                                           

<div class="frm-row ">						
<div class="section colm colm12">
<h2 >The Fun Part</h2>
</div>

<div class="section colm colm12">

<!-- Color HTML -->
<div class="cs-selectbox boxes-select  wow zoomIn" style="visibility: visible; animation-name: zoomIn;">

<div id="color_boxes_step4" class="cs-select cs-skin-boxes cs-active" tabindex="0">
<div class="boxesPreview"><img id="colboxes_preview" src="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-10T24.png?v=1511009727" border="0"><span id="colorName_Selected">Ombre 10t24</span></div>
<span id="color_boxes_trigger" class="cs-placeholder">Ombre 10t24</span>
<div class="cs-options">
<ul id="step4boxes_ul">	
	
	<?php 
		$redux_colorBg = $redux_demo['step4_color_main'];
		$redux_color_price = $redux_demo['step4_color_price'];
		
		for($n = 0; $n <= 25; $n++){
			$url_key = 'step4_color'.$n;
			$url_info = pathinfo($redux_demo[$url_key]['url']);
			$color_name = $url_info['filename'];
			?><li class="" data-price="<?php echo $redux_demo['step4_color_price'][$n] ?>" style="background-image: url(<?php echo $redux_demo[$url_key]['url']; ?>);" data-image="<?php echo $redux_demo[$url_key]['url']; ?>" color-name="<?php echo $color_name; ?>" data-value="<?php echo $n;?>"><span><?php echo $color_name; ?></span></li>
			<?php	
			
		} 
	?>
	
	<!--							
	<li class="swatch-1-jet-black" style="background-image: url(<?php// echo $redux_demo['step4_color1']['url']; ?>);" data-price="<?php// echo $redux_demo['step4_color_price'][0]; ?>" data-value="1 Jet Black"><span>1 Jet Black</span></li>
	<li class="swatch-1b-natural-black" data-option="" data-value="1b Natural Black"><span>1b Natural Black</span></li>
	<li class="swatch-2-darkest-brown" data-option="" data-value="2 Darkest Brown"><span>2 Darkest Brown</span></li>
	<li class="swatch-4-dark-brown" data-option="" data-value="4 Dark Brown"><span>4 Dark Brown</span></li>
	<li class="swatch-8-light-chestnut-brown" data-option="" data-value="8 Light Chestnut Brown"><span>8 Light Chestnut Brown</span></li>
	<li class="swatch-30-auburn" data-option="" data-value="30 Auburn"><span>30 Auburn</span></li>
	<li class="swatch-99j-plum" data-option="" data-value="99j Plum"><span>99j Plum</span></li>
	<li class="swatch-613-blonde" data-option="" data-value="613 Blonde"><span>613 Blonde</span></li>
	<li class="swatch-613-platinum-blonde" data-option="" data-value="613 Platinum Blonde"><span>613 Platinum Blonde</span></li>
	<li class="swatch-ash-blonde" data-option="" data-value="Ash Blonde"><span>Ash Blonde</span></li>
	<li class="swatch-blue-steel" data-option="" data-value="Blue Steel"><span>Blue Steel</span></li>
	<li class="swatch-burgundy-bistro" data-option="" data-value="Burgundy Bistro"><span>Burgundy Bistro</span></li>
	<li class="swatch-ruby-red" data-option="" data-value="Ruby Red"><span>Ruby Red</span></li>
	<li class="swatch-burgundy" data-option="" data-value="Burgundy"><span>Burgundy</span></li>
	<li class="swatch-copper" data-option="" data-value="Copper"><span>Copper</span></li>
	<li class="swatch-light-blonde" data-option="" data-value="Light Blonde"><span>Light Blonde</span></li>
	<li class="swatch-mahogany" data-option="" data-value="Mahogany"><span>Mahogany</span></li>
	<li class="swatch-mystic-turquoise" data-option="" data-value="Mystic Turquoise"><span>Mystic Turquoise</span></li>
	<li class="swatch-ombre-1b30" data-option="" data-value="Ombre 1b30"><span>Ombre 1b30</span></li>
	<li class="swatch-ombre-1bt10" data-option="" data-value="Ombre 1bt10"><span>Ombre 1bt10</span></li>
	<li class="swatch-ombre-2t27" data-option="" data-value="Ombre 2t27"><span>Ombre 2t27</span></li>
	<li class="swatch-ombre-4t30" data-option="" data-value="Ombre 4t30"><span>Ombre 4t30</span></li>
	<li class="swatch-ombre-6t27" data-option="" data-value="Ombre 6t27"><span>Ombre 6t27</span></li>
	<li class="swatch-ombre-10t24 cs-selected" data-option="" data-value="Ombre 10t24"><span>Ombre 10t24</span></li>
	<li class="swatch-ombre-18t22" data-option="" data-value="Ombre 18t22"><span>Ombre 18t22</span></li>
	<li class="swatch-pink-pearl" data-option="" data-value="Pink Pearl"><span>Pink Pearl</span></li>
	<li class="swatch-purple-rain" data-option="" data-value="Purple Rain"><span>Purple Rain</span></li>
	<li class="swatch-silver" data-option="" data-value="Silver"><span>Silver</span></li>
	<li class="swatch-silver-grey-black" data-option="" data-value="Silver Grey Black"><span>Silver Grey Black</span></li>
	<li class="swatch-ultra-violet" data-option="" data-value="Ultra Violet"><span>Ultra Violet</span></li> -->
</ul>
</div>

<select class="cs-select cs-skin-boxes" data-required="1" data-alert="Wait! Select a color first..." data-picked="0">

<option value="" disabled="" selected="">Pick your color</option>
 <option value="1 Jet Black" data-price="200.00" data-class="swatch-1-jet-black" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/1-jet-black.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/1-jet-black.png?v=1511009486" data-variant-id="410023034900">1 Jet Black</option>

<option value="1b Natural Black" data-price="200.00" data-class="swatch-1b-natural-black" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/1b-natural-black.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/1b-natural-black.png?v=1511009486" data-variant-id="410023100436">1b Natural Black</option>

<option value="2 Darkest Brown" data-price="200.00" data-class="swatch-2-darkest-brown" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/2-darkest-brown.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/2-darkest-brown.png?v=1511009486" data-variant-id="410023133204">2 Darkest Brown</option>

<option value="4 Dark Brown" data-price="200.00" data-class="swatch-4-dark-brown" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/4-dark-brown.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/4-dark-brown.png?v=1511009486" data-variant-id="410023165972">4 Dark Brown</option>

<option value="8 Light Chestnut Brown" data-price="200.00" data-class="swatch-8-light-chestnut-brown" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/8-light-chestnut-brown.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/8-light-chestnut-brown.png?v=1511009486" data-variant-id="410023198740">8 Light Chestnut Brown</option>

<option value="30 Auburn" data-price="200.00" data-class="swatch-30-auburn" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/30-auburn.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/30-auburn.png?v=1511009486" data-variant-id="410023231508">30 Auburn</option>

<option value="99j Plum" data-price="200.00" data-class="swatch-99j-plum" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/99J-plum.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/99J-plum.png?v=1511009486" data-variant-id="410023264276">99j Plum</option>

<option value="613 Blonde" data-price="200.00" data-class="swatch-613-blonde" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/613-blonde.png?v=1511009486" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/613-blonde.png?v=1511009486" data-variant-id="410023297044">613 Blonde</option>

<option value="613 Platinum Blonde" data-price="200.00" data-class="swatch-613-platinum-blonde" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/613-platinum-blonde.png?v=1511009496" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/613-platinum-blonde.png?v=1511009496" data-variant-id="410023329812">613 Platinum Blonde</option>

<option value="Ash Blonde" data-price="200.00" data-class="swatch-ash-blonde" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ash-blonde.png?v=1511009502" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ash-blonde.png?v=1511009502" data-variant-id="410023362580">Ash Blonde</option>

<option value="Blue Steel" data-price="200.00" data-class="swatch-blue-steel" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/blue-steel.png?v=1511009508" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/blue-steel.png?v=1511009508" data-variant-id="410023395348">Blue Steel</option>

<option value="Burgundy Bistro" data-price="200.00" data-class="swatch-burgundy-bistro" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/burgundy-bistro.png?v=1511009515" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/burgundy-bistro.png?v=1511009515" data-variant-id="410023428116">Burgundy Bistro</option>

<option value="Ruby Red" data-price="200.00" data-class="swatch-ruby-red" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ruby-red.png?v=1511009583" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ruby-red.png?v=1511009583" data-variant-id="418989735956">Ruby Red</option>

<option value="Burgundy" data-price="200.00" data-class="swatch-burgundy" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/burgundy.png?v=1511009522" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/burgundy.png?v=1511009522" data-variant-id="410023460884">Burgundy</option>

<option value="Copper" data-price="200.00" data-class="swatch-copper" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/copper.png?v=1511009531" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/copper.png?v=1511009531" data-variant-id="410023493652">Copper</option>

<option value="Light Blonde" data-price="200.00" data-class="swatch-light-blonde" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/light-blonde.png?v=1511009557" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/light-blonde.png?v=1511009557" data-variant-id="410023526420">Light Blonde</option>

<option value="Mahogany" data-price="200.00" data-class="swatch-mahogany" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/mahogany.png?v=1511009568" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/mahogany.png?v=1511009568" data-variant-id="410023559188">Mahogany</option>

<option value="Mystic Turquoise" data-price="200.00" data-class="swatch-mystic-turquoise" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/mystic-turquoise.png?v=1511009606" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/mystic-turquoise.png?v=1511009606" data-variant-id="410023624724">Mystic Turquoise</option>

<option value="Ombre 1b30" data-price="200.00" data-class="swatch-ombre-1b30" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-1B30.png?v=1511009623" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-1B30.png?v=1511009623" data-variant-id="410023657492">Ombre 1b30</option>

<option value="Ombre 1bt10" data-price="200.00" data-class="swatch-ombre-1bt10" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-1BT10.png?v=1511009634" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-1BT10.png?v=1511009634" data-variant-id="410023690260">Ombre 1bt10</option>

<option value="Ombre 2t27" data-price="200.00" data-class="swatch-ombre-2t27" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-2T27.png?v=1511009690" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-2T27.png?v=1511009690" data-variant-id="410023723028">Ombre 2t27</option>

<option value="Ombre 4t30" data-price="200.00" data-class="swatch-ombre-4t30" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-4T30.png?v=1511009706" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-4T30.png?v=1511009706" data-variant-id="410023755796">Ombre 4t30</option>

<option value="Ombre 6t27" data-price="200.00" data-class="swatch-ombre-6t27" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-6T27.png?v=1511009718" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-6T27.png?v=1511009718" data-variant-id="410023788564">Ombre 6t27</option>

<option value="Ombre 10t24" data-price="200.00" data-class="swatch-ombre-10t24" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-10T24.png?v=1511009727" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-10T24.png?v=1511009727" data-variant-id="410023821332">Ombre 10t24</option>

<option value="Ombre 18t22" data-price="200.00" data-class="swatch-ombre-18t22" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-18T22.png?v=1511009741" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ombre-18T22.png?v=1511009741" data-variant-id="410023854100">Ombre 18t22</option>

<option value="Pink Pearl" data-price="200.00" data-class="swatch-pink-pearl" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/pink-pearl.png?v=1511009753" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/pink-pearl.png?v=1511009753" data-variant-id="410023886868">Pink Pearl</option>

<option value="Purple Rain" data-price="200.00" data-class="swatch-purple-rain" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/purple-rain.png?v=1511009762" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/purple-rain.png?v=1511009762" data-variant-id="410023919636">Purple Rain</option>

<option value="Silver" data-price="200.00" data-class="swatch-silver" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/silver-fox.png?v=1511009864" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/silver-fox.png?v=1511009864" data-variant-id="410024083476">Silver</option>

<option value="Silver Grey Black" data-price="200.00" data-class="swatch-silver-grey-black" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/silver-grey-black.png?v=1511009873" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/silver-grey-black.png?v=1511009873" data-variant-id="410024116244">Silver Grey Black</option>

<option value="Ultra Violet" data-price="200.00" data-class="swatch-ultra-violet" data-image="https://cdn.shopify.com/s/files/1/2299/3373/products/ultra-violet.png?v=1511009819" data-thumb="https://cdn.shopify.com/s/files/1/2299/3373/products/ultra-violet.png?v=1511009819" data-variant-id="410024149012">Ultra Violet</option>


</select></div></div>


<!-- Color HTML -->

</div>
</div>

<div class="frm-row">
<div class="section colm colm3">
</div>
<div id="colorval_main" class="section colm colm6">
	<label id="color_validation_notice" class="field prepend-icon">
		<input style="visibility:hidden;height:0px;" type="text" name="colorvalidation" value="" id="colorvalidation" class="required gui-input" placeholder="Color validation" required>		
	</label>
</div>
<div class="section colm colm3">
</div>
</div>

</fieldset> 

<h2>Review</h2>
<fieldset>

<!-- STEP 5 -->                                           

<div class="frm-row ">						
<div class="section colm colm12">
<h2 class="">Review Your Wig Order</p>
</div>

<div class="section colm colm12">

<!-- Review Order Html -->
<div class="summary-list">

<div class="row summery-box wow slideInUp" data-summary="model" style="visibility: visible; animation-name: slideInUp;">
<div class="heading-box"><img src="http://wig.logicsbuffer.com/wp-content/uploads/2018/02/wig-photo-title.jpg" border="0"></div>
<div class="content-box">
<div class="imgbox"><img id="wig_photosummry" src="uploads/1517897372.png" border="0" class="img-responsive"></div>
<div class="content">
<h1 class="title"><span class="yellow_text">Your Wig Photo</span></h1>
<p>Alonzo Arnold will create your wig<br>based off the photo you have provided.</p>
</div>
<div class="clearfix"></div>                          
</div>
</div>

<div class="row summery-box wow slideInUp" data-summary="length" style="visibility: visible; animation-name: slideInUp;">                            
<div class="content-box">
<div class="heading-box"><img src="http://wig.logicsbuffer.com/wp-content/uploads/2018/02/length-title.jpg" border="0" alt="Model Length Title"></div>
<div class="imgbox"><img src="http://wig.logicsbuffer.com/wp-content/uploads/2018/02/length-summery.png" alt="Model Length" border="0" class="img-responsive"></div>
<div class="content">
<h1 class="title"><span class="yellow_text">Length:</span> <span class="length-label"><span id="length_val"></span>" - $<span id="length_price"></span></span> </h1>
<p>This is how long the hair for your wig will be.<br>*Wig total price may vary depending on desire length.</p>
</div>
<div class="clearfix"></div>                          
</div>
</div>

<div class="row summery-box wow slideInUp" data-summary="texture" style="visibility: visible;">
<div class="heading-box"><img src="http://wig.logicsbuffer.com/wp-content/uploads/2018/02/texture-title.jpg" alt="Model Texture Title" border="0"></div>
<div class="content-box">
<div class="imgbox" ><img id="selected_textureImg" src="https://cdn.shopify.com/s/files/1/2299/3373/products/1.png?v=1510960026" border="0" class="img-responsive"></div>
<div class="content">
<h1 class="title"><span class="yellow_text">Texture: </span><span class="texture-label">Body Wave - $<span id="sel_textureprice"></span></span> </h1>
<p>This is how long the hair for your wig will be.<br>*Wig total price may vary depending on desire length.</p>
</div>
<div class="clearfix"></div>                          
</div>
</div>

<div class="row summery-box wow slideInUp" data-summary="model-color" style="visibility: visible;">
<div class="heading-box"><img src="http://wig.logicsbuffer.com/wp-content/uploads/2018/02/color-title.jpg" alt="Model Color Title" border="0"></div>
<div class="content-box">
<div class="imgbox"><img id="selected_colorImg" src="https://cdn.shopify.com/s/files/1/2299/3373/products/99J-plum.png?v=1511009486" alt="Model Color" border="0" class="img-responsive"></div>
<div class="content">
<h1 class="title"><span class="yellow_text">Color: </span><span class="boxes-label"><span id="sel_colorName"></span> - $<span id="sel_colorprice"></span></span> </h1>
<p>This color treatment will be applied to the hair of your wig.</p>
</div>
<div class="clearfix"></div>                          
</div>
</div>

</div>
<!-- Review Order Html End -->

</div>
</div>

</fieldset> 
<!-- STEP 6 -->                                           


<h2>Pay Now</h2>
<fieldset>


<div class="frm-row ">						
<div class="section colm colm12">
<h2 class="">Pay Now by Clicking Pay now Button</h2>
</div>
</div>
<!--
<div class="frm-row ">	
	<div class="colm colm2">
	</div>
	<div class="colm colm8">
	<div class="section">
		<label for="username" class="field prepend-icon">
		<input type="text" name="userName" placeholder="Full Name" class="gui-input required" id="username" value=""  required> 
		<b class="tooltip tip-right-top"><em> Enter Your Full Name Here.</em></b>
		<span class="field-icon"><i class="fa fa-user"></i></span>  					
		</label>
	</div>
	</div>
	<div class="colm colm2">
	</div>
</div>

<div class="frm-row ">
	<div class="colm colm2">
	</div>	
	<div class="colm colm8">
	<div class="section">
	   <label for="email" class="field prepend-icon">
		<input type="text" name="email" placeholder="Email" class="gui-input required" id="email" value=""  required> 
		<b class="tooltip tip-right-top"><em> Enter Your Email Here.</em></b>
		<span class="field-icon"><i class="fa fa-envelope"></i></span>  					
		</label>
	</div>
	</div>
	<div class="colm colm2">
	</div>
</div>  -->

<div class="frm-row ">	
	<div class="colm colm2">
	</div>
	<div class=" colm colm8">                    
	<div class="section">
		<input id="pay_now_btn" type="button" value="Pay Now" onclick="send_ajax()" class="">
	</div>
	</div>
	<div class="colm colm2">
	</div>
</div> 

</fieldset>
 
</form>
	<div class="tot-price">
        Total : 
        <span class="yellow_text">$<b id="total_priceshow">0.00</b></span>
    </div>
	
	
</div>
</div>

<script>

function show_price_image(){
		var image_price = jQuery("#upload_imgPrice").val();			
		var total_price;		
		total_price = parseFloat(image_price);						
		
		jQuery("#total_priceshow").html(total_price);
		jQuery("#total_price_image").val(total_price);
		
		var get_image64bit = jQuery(".dz-image img").attr('src');
		console.log(get_image64bit);
		localStorage.setItem("selected_imageCode",get_image64bit);
		jQuery("#select_photo").css("background-image", "url('" + get_image64bit + "')");
		jQuery("#select_photo").html("");
		jQuery('#imagevalidation').val("validated");
		jQuery("#wig_photosummry").attr('src', get_image64bit);
		
}

function show_price_length(length_val){
		var length_val = length_val;
		console.log("length_val.."+length_val);		
		var price_previous = jQuery("#total_price_image").val();			
		var total_price = parseFloat(price_previous) + parseFloat(length_val);				
		
		jQuery("#length_price").html(length_val);
		jQuery("#total_priceshow").html(total_price);
	    jQuery("#total_price_length").val(length_val);		
		jQuery('#lengthvalidation').val("validated");
}

function show_price_texture(texture_val){
		var texture_val = texture_val;
		console.log("texture_val.."+texture_val);
		var price_image = jQuery("#total_price_image").val();			
		var price_length = jQuery("#total_price_length").val();			
		var total_pricetexture = parseFloat(price_image) + parseFloat(price_length) + parseFloat(texture_val);						
		jQuery("#total_priceshow").html(total_pricetexture);
	    jQuery("#total_price_texture").val(texture_val);
		jQuery('#texturevalidation').val("validated");		
}

function show_price_color(selected_price){
		var selected_price = selected_price;
		console.log("selected_price.."+selected_price);
		var price_image = jQuery("#total_price_image").val();			
		var price_length = jQuery("#total_price_length").val();			
		var price_texture = jQuery("#total_price_texture").val();			
		var total_pricetexture = parseFloat(price_image) + parseFloat(price_length) + parseFloat(price_texture) + parseFloat(selected_price);				
		
		jQuery("#sel_colorprice").html(selected_price);
		jQuery("#total_priceshow").html(total_pricetexture);
	    jQuery("#total_price_color").val(selected_price);	
		showPayNowPrice();		
		jQuery('#colorvalidation').val("validated");
}

function showPayNowPrice(){	
	var price_image = jQuery("#total_price_image").val();
	var price_length = jQuery("#total_price_length").val();
	var price_texture = jQuery("#total_price_texture").val();
	var price_color = jQuery("#total_price_color").val();
	var final_price = parseFloat(price_image) + parseFloat(price_length) + parseFloat(price_texture) + parseFloat(price_color);
	jQuery("#total_price_sum").val(final_price);
	jQuery("#show_totalPayment").html(final_price);
}
function step_changing(){
/* 	var image_validation = jQuery('#imagevalidation').val;
	if(image_validation == ""){
        console.log("inside image val"); 		
		alert("Please Upload Image First.");
	} */
}

jQuery(document).ready(function() {	

var slider = document.getElementById("myRange");
var output = document.getElementById("length_chosen");
output.innerHTML = slider.value;

  slider.oninput = function() {
  output.innerHTML = this.value;
  var current_val = this.value; 
  var listItems_slider = jQuery("#lenth_values li");
  var dummy_girl_ul = jQuery("#dummy_girl li");
  localStorage.setItem("selected_length",current_val);
  console.log("current_val..."+current_val);
  
  // Range Slider Code
  listItems_slider.each(function(idx, li) {
	var length_li = jQuery(li);
	var id1 = length_li.attr('id');	
	var selectorid1 = "#"+id1;	
		jQuery(selectorid1).removeClass("length-active");
	
  });

  listItems_slider.each(function(idx, li) {
	var length_li = jQuery(li);
	var id = length_li.attr('id');
	var length_val = length_li.attr('value');
		
	var selectorid = "#"+id;	
	
	if(current_val == id ){
		jQuery("#length_val").html(current_val);
		//console.log(current_val);
		console.log("Selected Val.."+length_val);
		show_price_length(length_val);
		jQuery(selectorid).addClass("length-active");
	}	
  });
  
  //Dummy Wig Code
  dummy_girl_ul.each(function(idx, li1) {
	var lady_li = jQuery(li1);
	var id1 = lady_li.attr('data-length-label');
	console.log("id1.."+id1);
	var sel_lady = "#l-"+id1;	
		jQuery(sel_lady).removeClass("line_selected");
	
  });

  dummy_girl_ul.each(function(idx, li1) {
	var lady_li = jQuery(li1);
	var id = lady_li.attr('data-length-label');	
	var sel_lady1 = "#l-"+id;	
	
	if(current_val == id ){
		console.log(current_val);
		jQuery(sel_lady1).addClass("line_selected");
	}	
  });   
}

jQuery(function() {       
    jQuery('#smart-form').validate();
            
 }); 
	
//Step Changing

	
//Step 3 Texture Toggle
jQuery("#cs_select_texture").removeClass("cs-active");

jQuery( ".cs-placeholder" ).click(function() {
	jQuery("#cs_select_texture").toggleClass("cs-active");
});

//Step 4 Color
jQuery("#color_boxes_step4").removeClass("cs-active");

jQuery( "#color_boxes_trigger" ).click(function() {
	jQuery("#color_boxes_step4").toggleClass("cs-active");
});

jQuery('#texture_images_ul li').click(function(e) {
    jQuery(this).addClass('texture-selected').siblings().removeClass('texture-selected');
	var texture_val = jQuery(this).attr('data-price');
	var texture_name = jQuery(this).attr('data-content');
	localStorage.setItem("sel_texture_name",texture_name);
	var sel_texture_price = jQuery(this).attr('data-price');
	console.log("texture_val..."+texture_val);
	show_price_texture(texture_val);
	var texture_selected = jQuery(this).attr('image-url');
	jQuery("#cs_select_texture .cs-placeholder").css("background-image", "url('" + texture_selected + "')");	
	jQuery("#selected_textureImg").attr("src",texture_selected);
	jQuery("#sel_textureprice").html(sel_texture_price);
	jQuery("#cs_select_texture").removeClass("cs-active");
	
});

jQuery('#step4boxes_ul li').click(function(e) {
    e.preventDefault();
    jQuery('#step4boxes_ul li').removeClass('cs-selected');
    jQuery(this).addClass('cs-selected');
    setTimeout(hide_active_step4, 1000);
	var selected_color = jQuery(this).attr('data-image');	
	var selected_colorName = jQuery(this).attr('color-name');	
	jQuery("#selected_colorImg").attr("src",selected_color);
	jQuery("#colboxes_preview").attr("src",selected_color);
	jQuery("#colorName_Selected").html(selected_colorName);
	jQuery("#sel_colorName").html(selected_colorName);
	localStorage.setItem("sel_colorName", selected_colorName);
	
	var selected_price = jQuery(this).attr('data-price');
	show_price_color(selected_price);
});

function hide_active_step4(){
	jQuery("#color_boxes_step4").removeClass('cs-active');	
}

// Dropzone

 var myDropzone = new Dropzone("#myDropzone",{ 
	url: '/',
	autoProcessQueue: false,
	uploadMultiple: true,
	parallelUploads: 1,
	maxFiles: 1,
	maxFilesize: 1,
	paramName: "file",
	dictDefaultMessage: '',
	acceptedFiles: ".jpeg,.jpg,.png,.gif",
	addRemoveLinks: true,
	dictRemoveFile: '&#215;',
	init: function () {
      this.on('sending', dz_sending),
      this.on('success', dz_success),
      this.on('error', dz_error),
      this.on('complete', dz_complete) // Once it's done...
    }
});

function dz_complete(file) {
  console.log("file 64Bit code");
  //console.log(file);
  jQuery('.dz-preview').remove();  // ...delete the template gen'd html.
}
function dz_sending(file) {
  //console.log(file);
}
function dz_success(file) {
  //console.log(file);
}
function dz_error(file) {
  //console.log(file);
}


});
function send_ajax() {
    var final_price = jQuery("#total_price_sum").val();
	console.log("final_price.."+final_price);
	var user_name = jQuery("#username").val();
	var user_email = jQuery("#email").val();
	var domain_link = document.domain;
	var price_image = jQuery("#total_price_image").val();
	var price_length = jQuery("#total_price_length").val();
	var price_texture = jQuery("#total_price_texture").val();
	var price_color = jQuery("#total_price_color").val();
	var sel_colorName = localStorage.getItem("sel_colorName");
	var sel_imgCode = localStorage.getItem("selected_imageCode");
	var sel_lengthVal = localStorage.getItem("selected_length");
	var sel_texture_name = localStorage.getItem("sel_texture_name");
	
	console.log("sel_imgCode.."+sel_imgCode);
		
	var checkout = "checkout";
	var domain_link1 = domain_link+'/'+checkout;
	//console.log("domain_link1.."+domain_link1);
	
	jQuery.ajax({
		url : postlove.ajax_url,
		type : 'post',
		data : {
			action : 'my_custom_wig',
			'getPriceAjax' : 	true,
			'addToCart' : 		true,
			'wig_price' : 	final_price,
			'wig_username' :  user_name,
			'wig_email' :  user_email,	
			'price_image' :  price_image,	
			'price_length' :  price_length,	
			'price_texture' :  price_texture,	
			'price_color' :  price_color,	
			'sel_colorName' :  sel_colorName,	
			'sel_ImageCode' :  sel_imgCode,	
			'sel_lengthVal' :  sel_lengthVal,	
			'sel_texture_name' :  sel_texture_name,	
		},
		success : function( response ) {
			console.log(response);
			console.log("domain_link1.."+domain_link1);
			//window.location.href(domain_link);
			//jQuery(location).attr('href',domain_link);
			//window.location(domain_link);
			//window.open("http://www.google.com","_self");
			document.location.href=domain_link1;
		}
	});
}
</script>			
</div>
<?php
return ob_get_clean();
}

add_action( 'wp_enqueue_scripts', 'ajax_test_enqueue_scripts' );
function ajax_test_enqueue_scripts() {
	if( is_single() ) {
		wp_enqueue_style( 'love', plugins_url( '/love.css', __FILE__ ) );
	}

	wp_enqueue_script( 'love', plugins_url( '/love.js', __FILE__ ), array('jquery'), '1.0', true );

	wp_localize_script( 'love', 'postlove', array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
	));

}
add_action( 'wp_ajax_nopriv_my_custom_wig', 'run_my_custom_wig' );
add_action( 'wp_ajax_post_my_custom_wig', 'run_my_custom_wig' );

function run_my_custom_wig() {
	$love = $_REQUEST['wig_price'];
	$price_image = $_REQUEST['price_image'];
	$price_length = $_REQUEST['price_length'];
	$price_texture = $_REQUEST['price_texture'];
	$price_color = $_REQUEST['price_color'];
	$sel_colorName = $_REQUEST['sel_colorName'];
	$sel_ImageCode = $_REQUEST['sel_ImageCode'];
	$sel_lengthVal = $_REQUEST['sel_lengthVal'];
	$sel_texture_name = $_REQUEST['sel_texture_name'];
			
	if($price_image){
		update_option('price_image',$price_image);
	}
	if($price_length){
		update_option('price_length',$price_length);
	}
	if($price_texture){
		update_option('price_texture',$price_texture);
	}
	if($price_color){
		update_option('price_color',$price_color);
		//update_option('custom_wig',"true");
	}	
	if($sel_colorName){
		update_option('color_name',$sel_colorName);
	}
	/* if($sel_ImageCode){
		update_option('sel_ImageCode',$sel_ImageCode);
	} */	
	if($sel_lengthVal){
		update_option('sel_lengthVal',$sel_lengthVal);
	}
	if($sel_texture_name){
		update_option('sel_texture_name',$sel_texture_name);
	}
	
	
	//Image link Creation
	
	$img_64 = $_REQUEST['sel_ImageCode'];
	$emoji_reaction = $_REQUEST['emoji_reaction'];
	$design_name = $_REQUEST['canvas_img1'];
	$react_post_id = $_REQUEST['c_post_id'];
	$file_type='.jpeg';
	$current_user='upload_images';
	$upload_dir = wp_upload_dir();
	
	$user_dirname = $upload_dir['basedir'].'/'.$current_user;
	if ( ! file_exists( $user_dirname ) ) {
		wp_mkdir_p( $user_dirname );
	}
		
	$path = $user_dirname.'/'.$sel_colorName.$file_type;
	//print_r($path);
	//print_r($design_name);
	$ifp = fopen($path, "wb"); 
    $data = explode(',', $img_64);
    fwrite($ifp, base64_decode($data[1])); 
    fclose($ifp);	

	//Insert Post 
	global $post;
	if($path){
		$uniq_id = uniqid();

		// Create post object				
			$my_post = array(
			  'post_title'    => "Wig $uniq_id",
			  'post_content'  => "Wig uploaded Image",
			  'post_status'   => 'publish'
			);
		
		// Insert the post into the database
		$parent_post_id = wp_insert_post( $my_post );
	}
	//$user_id = get_post_field( 'post_author', $parent_post_id );
	//$fb_url = get_avatar_url( $user_id, 72 );
	//$fb_img = get_usermeta($user_id, 'facebook_avatar_thumb');
	//$size = 72;
	/* $fb_id = get_user_meta( $user_id, '_fb_user_id', true );
	if ( $fb_id = get_user_meta( $user_id, '_fb_user_id', true ) ) {
		$fb_url = 'https://graph.facebook.com/' . $fb_id . '/picture?width=' . $size . '&height=' . $size;
	} */
	//$reaction_id = "reaction_".$react_post_id;
	//$category_terms = array( $reaction_id );
	//wp_set_object_terms( $parent_post_id, $category_terms , 'category' );
	//wp_set_post_terms( $parent_post_id, array('metais'), $category_texonomy);
	
	// Check the type of file. We'll use this as the 'post_mime_type'.
	$filetype = wp_check_filetype( basename( $path ), null );

	// Get the path to the upload directory.
	$wp_upload_dir = wp_upload_dir();

	// Prepare an array of post data for the attachment.
	$attachment = array(
		'guid'           => $wp_upload_dir['url'] . '/' . basename( $path ), 
		'post_mime_type' => $filetype['type'],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $path ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);
	// Insert the attachment.
	$attach_id = wp_insert_attachment( $attachment, $path, $parent_post_id );
	$image_url = wp_get_attachment_url( $attach_id );	
	
	session_start();  	
	$_SESSION["wig_imgCode"]  = $image_url;
	
	//Image link Creation
	
	WC()->cart->empty_cart(); 
	//$post_id1 = get_option('stern_taxi_fare_product_id_wc');
	$post_id1   = 2781;    
    $custom_PriceFinal = $love;
	

	$wig_prod_id = wp_insert_post( array(
		'post_title' => 'Hair Wig',
		'post_content' => 'Here is content of the post, so this is our great new products description',
		'post_status' => 'publish',
		'post_type' => "product",
	) );
	wp_set_object_terms( $wig_prod_id, 'simple', 'product_type' );
	
	update_post_meta( $wig_prod_id, '_regular_price', $custom_PriceFinal);
	update_post_meta($wig_prod_id, '_price', $custom_PriceFinal);	
	
	
	//check if product already in cart
	//if ( WC()->cart->get_cart_contents_count() == 0 ) {
		// if no products in cart, add it
		WC()->cart->add_to_cart( $wig_prod_id );
	//}
	die();
}