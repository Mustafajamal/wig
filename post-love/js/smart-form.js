jQuery(document).ready(function($){
			
			$("#smart-form").steps({
				bodyTag: "fieldset",
				headerTag: "h2",
				bodyTag: "fieldset",
				transitionEffect: "slideLeft",
				titleTemplate: "<span class='number'>#index#</span> #title#",
				labels: {
					finish: "Pay Now",
					next: "Continue",
					previous: "Go Back",
					loading: "Loading..." 
				},
				onStepChanging: function (event, currentIndex, newIndex){
					//step_changing();
					
					if (currentIndex > newIndex){return true; }
					var form = $(this);
					if (currentIndex < newIndex){
						var image_validation = jQuery('#imagevalidation').val;
						if(image_validation == ""){
							console.log("inside image val"); 		
							alert("Please Upload Image First.");
						}
					
					}
					return form.valid();
					//console.log("newIndex.."+newIndex);
					
				},
				onStepChanged: function (event, currentIndex, priorIndex){
					
				},
				onFinishing: function (event, currentIndex){											
					var form = $(this);
					form.validate().settings.ignore = ":disabled";
										
						//Form Validation Check						
						var total_price_image = jQuery('#total_price_image').val();
						var total_price_length = jQuery('#total_price_length').val();
						var total_price_texture = jQuery('#total_price_texture').val();
						var total_price_color = jQuery('#total_price_color').val();
						var total_price_sum = jQuery('#total_price_sum').val();
						var username = jQuery('#username').val();
						var email = jQuery('#email').val();
						var form_valid;
						
						
						if(username == ""){
						 console.log("form Invalid");
						 form_valid = false;						 
						}
						if(email == ""){
						 console.log("form Invalid");
						 form_valid = false;						 
						}
						if(total_price_image == 0.00){
						 console.log("form Invalid");
						 form_valid = false;						 
						}
						if(total_price_length == 0.00){
						 console.log("form Invalid");
						 form_valid = false;						 
						}
						if(total_price_texture == 0.00){
						 console.log("form Invalid");
						 form_valid = false;						 
						}
						if(total_price_color == 0.00){
						 console.log("form Invalid");
						 form_valid = false;						 
						}
						if(total_price_sum == 0.00){
						 console.log("form Invalid");
						 form_valid = false;						 
						}
																								
						else{
						 form_valid = true;	
						 console.log("Form Valid");
						}
						console.log("form_valid..."+form_valid);
						
					 if (form_valid == true){
						 //console.log("error_array..."+error_array);
						//jQuery( "#step4" ).show();
						//jQuery( ".smart-wrap" ).hide();
						//jQuery( ".form_desc" ).hide();
						//setval_info();
						//var value_set_info = jQuery( "#vehicle_val" ).val();				
						//jQuery( "#cars_worth" ).html(value_set_info);
						send_ajax();	
						var domain_link = document.domain;
						var checkout = "checkout";
						var domain_link1 = domain_link+'/'+checkout;
						document.location.href=domain_link1;
					 
					} 
					
					return form.valid();
					
					
				},
				onFinished: function (event, currentIndex){
					var form = $(this);
					$(form).ajaxSubmit({
							target:'.result',			   
							beforeSubmit:function(){
								
							},
							error:function(){
							},
							 success:function(){						
									$('.alert-success').show().delay(7000).fadeOut();
									$('.field').removeClass("state-error, state-success");
									if( $('.alert-error').length == 0){
										$('#smart-form').resetForm();
										reloadCaptcha();
									}
							 }
					  });					
				}
			}).validate({
				errorClass: "state-error",
				validClass: "state-success",
				errorElement: "em",
				onkeyup: false,
				onclick: false,
				rules: {
					imagevalidation: {
						required: true
					},
					lastname: {
						required: true
					},										
					emailaddress: {
						required: true,
						email: true
					},					
					services:{
						required: true
					},
					bugdet:{
						required: true
					},					
					captcha:{
						required:true,
						remote:'php/captcha/process.php'
					}					
				},
				messages: {
					firstname: {
						required: "Please enter firstname"
					},
					lastname: {
						required: "Please enter lastname"
					},
					emailaddress: {
						required: 'Please enter your email',
						email: 'You must enter a VALID email'
					},
					telephone: {
						required: 'Please enter your telephone',
						number: 'Please enter numbers only'
					},					
					project_title: {
						required: "Please enter the project title"
					},
					years_dltotal:{
						required: 'Please enter contact person'
					},
					services:{
						required: 'Please select services'
					},
					bugdet:{
						required: 'Please select project budget'
					},					
					captcha:{
							required: 'You must enter the captcha code',
							remote:'Captcha code is incorrect'
					}					
				},
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.field').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.field').removeClass(errorClass).addClass(validClass);
				},
				errorPlacement: function(error, element) {
					if (element.is(":radio") || element.is(":checkbox")) {
						element.closest('.option-group').after(error);
					} else {
						error.insertAfter(element.parent());
					}
				}
			
			});
			
			/* Reload Captcha
			----------------------------------------------- */	
			function reloadCaptcha(){ $("#captchax").attr("src","php/captcha/captcha.php?r=" + Math.random()); }
			$('.captcode').click(function(e){
				e.preventDefault();
				reloadCaptcha();
			});			
			
			/* Project datepicker range
			----------------------------------------------- */			
			$("#start_date").datepicker({
				defaultDate: "+1w",
				changeMonth: false,
				numberOfMonths: 1,
				prevText: '<i class="fa fa-chevron-left"></i>',
				nextText: '<i class="fa fa-chevron-right"></i>',
				onClose: function( selectedDate ) {
					$( "#end_date" ).datepicker( "option", "minDate", selectedDate );
				}
			});
			
			$("#end_date").datepicker({
				defaultDate: "+1w",
				changeMonth: false,
				numberOfMonths: 1,
				prevText: '<i class="fa fa-chevron-left"></i>',
				nextText: '<i class="fa fa-chevron-right"></i>',			
				onClose: function( selectedDate ) {
					$( "#start_date" ).datepicker( "option", "maxDate", selectedDate );
				}
			});
					
	}); 